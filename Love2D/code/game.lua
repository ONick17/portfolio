require("ball")
require("player")

Game = {}
Game.__index = Game

function Game:create()
    local game = {}
    setmetatable(game, Game)

    game.ai = true
    game.player_sizes = {10, 50}
    game.ball_sizes = {75, 75}
    game.mode = false
    game.gamend = false
    game.speed = 300*heightScale
    game.boost = 3
    game.speed_rot = 15
    game.boost_rot = 2
    game.max_angle = 70
    game.turn = 0
    game.start_timer = 0
    game.player1_start = Vector:create(play_zone_width/10 + play_zone_x, play_zone_height/2 + play_zone_y)
    game.player2_start = Vector:create(play_zone_width/10*9 + play_zone_x, play_zone_height/2 + play_zone_y)
    game.player1_keys = {"w", "s", "lshift"}
    game.player2_keys = {"up", "down", "rctrl"}
    game.player1_score = 0
    game.player2_score = 0
    game.ball = nil
    game.player1 = nil
    game.player2 = nil
    game.timer1_flag = true
    game.timer2_flag = true
    game.timer3_flag = true
    return game
end

function Game:change_positions()
    self.player1_start = Vector:create(play_zone_width/10 + play_zone_x, play_zone_height/2 + play_zone_y)
    self.player2_start = Vector:create(play_zone_width/10*9 + play_zone_x, play_zone_height/2 + play_zone_y)
    self.player1:change_positions(self.player1_start)
    self.player2:change_positions(self.player2_start)
end

function Game:create_ball()
    self.ball = Ball:create(game.ball_sizes)
end

function Game:create_players()
    self.player1 = Player:create(game.player1_start, game.player_sizes, game.player1_keys, -1, false)
    self.player2 = Player:create(game.player2_start, game.player_sizes, game.player2_keys, 1, false)
end

function Game:start(ai)
    menu.interact = false
    self.timer1_flag = true
    sounds.background:pause()
    if not self.mode then
        self.player2.ai = ai
        self.mode = true
        self.gamend = false
        self.start_timer = 0
        self.player1.location = self.player1_start
        self.player2.location = self.player2_start
        game.player1_score = 0
        game.player2_score = 0
        self.turn = self.ball:launch(Vector:create(300*widthScale, 0))
    end
end

function Game:spacepressed()
    if self.gamend then
        sounds.switch:play()
        self.player1.location = self.player1_start
        self.player2.location = self.player2_start
        self.turn = self.ball:launch(Vector:create(300*widthScale, 0))
        self.gamend = false
        self.start_timer = 0
    end
end

function Game:escapepressed()
    sounds.background:play()
    sounds.selected:stop()
    sounds.selected:play()
    self.mode = false
    self.gamend = false
    menu.interact = true
    main_page:load()
end

function Game:update(dt)
    if self.mode then
        if self.start_timer < 3 then
            self.start_timer = self.start_timer + dt
        else
            if not self.gamend then
                self.ball:update(dt)
                self.player1:update(dt)
                self.player2:update(dt)
                if self.ball:check_boundaries() then
                    self.gamend = true
                    -- self.timer1_flag = true
                    -- self.timer2_flag = true
                    -- self.timer3_flag = true
                    sounds.goal:play()
                    if self.turn == 1 then
                        self.player1_score = self.player1_score + 1
                    else
                        self.player2_score = self.player2_score + 1
                    end
                end
                if self.turn == 1 then
                    if self.ball:check_player(self.player2) then
                        self.turn = self.turn * -1
                        self.ball:add_speed()
                        self.ball:change_size()
                    end
                elseif self.turn == -1 then
                    if self.ball:check_player(self.player1) then
                        self.turn = self.turn * -1
                        self.ball:add_speed()
                        self.ball:change_size()
                    end
                end
            end
        end
    end
end

function digits_find(y_fix, score)
    local digit1_fix = y_fix*heightScale
    local digit2_fix = y_fix*heightScale
    local digit1
    local digit2
    if score > 99 then
        digit1 = 9
        digit2 = 9
        digit1_fix = 0
        digit2_fix = 0
    else
        digit1 = (score - score % 10) / 10
        if digit1 == 0 then
            digit1 = 10
        end
        if (digit1 == 9) or (digit1 == 10) then
            digit1_fix = 0
        elseif (digit1 == 5) or (digit1 == 6) or (digit1 == 7) or (digit1 == 8) then
            digit1_fix = (y_fix-1)*heightScale
        end

        digit2 = score % 10
        if digit2 == 0 then
            digit2 = 10
        end
        if (digit2 == 9) or (digit2 == 10) then
            digit2_fix = 0
        elseif (digit2 == 5) or (digit2 == 6) or (digit2 == 7) or (digit2 == 8) then
            digit2_fix = (y_fix-1)*heightScale
        end
    end
    return digit1, digit2, digit1_fix, digit2_fix
end

function Game:draw()
    local y_fix = 2
    if self.mode then
        if self.gamend then
            if self.turn == 1 then
                love.graphics.draw(p1Img, menu_zone_x+menu_zone_width/2-p1Img:getWidth()/2*widthScale,
                                    menu_zone_y+menu_zone_height/4-p1Img:getHeight()/2*heightScale,
                                    0, widthScale, heightScale)
            elseif self.turn == -1 then
                love.graphics.draw(p2Img, menu_zone_x+menu_zone_width/2-p2Img:getWidth()/2*widthScale,
                                    menu_zone_y+menu_zone_height/4-p2Img:getHeight()/2*heightScale,
                                    0, widthScale, heightScale)
            end
            love.graphics.draw(wonImg, menu_zone_x+menu_zone_width/2-wonImg:getWidth()/2*widthScale,
                                menu_zone_y+menu_zone_height/4*2-wonImg:getHeight()/2*heightScale,
                                0, widthScale, heightScale)
            love.graphics.draw(endImg, play_zone_x+play_zone_width/2-endImg:getWidth()/4*widthScale,
                                play_zone_y+play_zone_height/2-endImg:getHeight()/2*heightScale,
                                0, widthScale, heightScale)
        else
            love.graphics.draw(p1Img, menu_zone_x+menu_zone_width/4-p1Img:getWidth()/2*widthScale,
                                menu_zone_y+menu_zone_height/4-p1Img:getHeight()/2*heightScale,
                                0, widthScale, heightScale)
            love.graphics.draw(p2Img, menu_zone_x+menu_zone_width/4-p2Img:getWidth()/2*widthScale,
                                menu_zone_y+menu_zone_height/4*2-p2Img:getHeight()/2*heightScale,
                                0, widthScale, heightScale)

            local digit1, digit2, digit1_fix, digit2_fix = digits_find(y_fix, self.player1_score)
            love.graphics.draw(digitsImg, digitsImg_quads[digit1],
                                menu_zone_x+menu_zone_width/4*3-digits_frame_width*widthScale,
                                menu_zone_y+menu_zone_height/4-digits_frame_height/2*heightScale+digit1_fix,
                                0, widthScale, heightScale)
            love.graphics.draw(digitsImg, digitsImg_quads[digit2],
                                menu_zone_x+menu_zone_width/4*3,
                                menu_zone_y+menu_zone_height/4-digits_frame_height/2*heightScale+digit2_fix,
                                0, widthScale, heightScale)
            
            digit1, digit2, digit1_fix, digit2_fix = digits_find(y_fix, self.player2_score)
            love.graphics.draw(digitsImg, digitsImg_quads[digit1],
                                menu_zone_x+menu_zone_width/4*3-digits_frame_width*widthScale,
                                menu_zone_y+menu_zone_height/4*2-digits_frame_height/2*heightScale+digit1_fix,
                                0, widthScale, heightScale)
            love.graphics.draw(digitsImg, digitsImg_quads[digit2],
                                menu_zone_x+menu_zone_width/4*3,
                                menu_zone_y+menu_zone_height/4*2-digits_frame_height/2*heightScale+digit2_fix,
                                0, widthScale, heightScale)

            self.ball:draw()
            self.player1:draw()
            self.player2:draw()
        end
        if self.start_timer < 1 then
            -- love.graphics.print("3", play_zone_width/2 + play_zone_x, play_zone_height/2 + play_zone_y)
            love.graphics.draw(digitsImg, digitsImg_quads[3],
                                menu_zone_width/2 + menu_zone_x-digits_frame_width*widthScale/2,
                                menu_zone_height/4*3 + menu_zone_y-digits_frame_height/2*heightScale+y_fix*heightScale,
                                0, widthScale, heightScale)
        elseif self.start_timer < 2 then
            if (self.timer1_flag) then
                sounds.switch:stop()
                sounds.switch:play()
                self.timer1_flag = false
                self.timer2_flag = true
            end
            -- love.graphics.print("2", play_zone_width/2 + play_zone_x, play_zone_height/2 + play_zone_y)
            love.graphics.draw(digitsImg, digitsImg_quads[2],
                                menu_zone_width/2 + menu_zone_x-digits_frame_width*widthScale/2,
                                menu_zone_height/4*3 + menu_zone_y-digits_frame_height/2*heightScale+y_fix*heightScale,
                                0, widthScale, heightScale)
        elseif self.start_timer < 3 then
            if (self.timer2_flag) then
                sounds.switch:stop()
                sounds.switch:play()
                self.timer2_flag = false
                self.timer3_flag = true
            end
            -- love.graphics.print("1", play_zone_width/2 + play_zone_x, play_zone_height/2 + play_zone_y)
            love.graphics.draw(digitsImg, digitsImg_quads[1],
                                menu_zone_width/2 + menu_zone_x-digits_frame_width*widthScale/2,
                                menu_zone_height/4*3 + menu_zone_y-digits_frame_height/2*heightScale+y_fix*heightScale,
                                0, widthScale, heightScale)
        elseif (self.timer3_flag) then
            sounds.switch:stop()
            sounds.switch:play()
            self.timer3_flag = false
            self.timer1_flag = true
        end
    else
        love.graphics.draw(titleImg, play_zone_x, play_zone_y+play_zone_height/4*1, 0, widthScale, heightScale)
    end
end