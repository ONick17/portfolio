import cv2
import numpy as np
#import keras
from tensorflow import keras
import matplotlib.pyplot as plt

draw = False
model = keras.models.load_model('mnist.h5')


def draw_callback(event, x, y, flags, param):
    global draw
    if event == cv2.EVENT_MOUSEMOVE:
        if draw:
            cv2.circle(img, (x, y), 10, 200, -1)
    elif event == cv2.EVENT_LBUTTONDOWN:
        draw = True
    elif event == cv2.EVENT_LBUTTONUP:
        draw = False


img = np.zeros((512, 512), dtype="uint8")

cv2.namedWindow("MNIST")
cv2.setMouseCallback("MNIST", draw_callback)

while True:
    cv2.imshow("MNIST", img)
    key = cv2.waitKey(1) & 0xFF
    if key == ord('q'):
        break
    elif key == ord('m'):
        print("Recognize")
        img2 = cv2.resize(img, (28,28))
        img2 = img2 / 255.
        img2 = img2.reshape((1, img2.shape[0], img2.shape[1], 1))
        predictions = model.predict(img2)
        print("probability =", np.max(predictions))
        predictions = np.argmax(predictions)
        print(predictions)
        plt.imshow(img2[0])
        plt.show()
    elif key == ord('c'):
        print("Clear")
        img[:] = 0

cv2.destroyAllWindows()
