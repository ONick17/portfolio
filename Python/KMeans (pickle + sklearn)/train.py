import pickle
import numpy as np
from sklearn.cluster import KMeans

X = np.loadtxt("b.dat")
k_model = KMeans(n_clusters=3)
k_model.fit(X)
k_ans = k_model.predict(X)

fl = open("test2.pkl", 'wb')
pickle.dump(k_model, fl)
fl.close()
