'''
Программа, запускающая приложение без интерфейса (через командную строку).
Мало тестировалась, мало использовалась, плохо прописана.
Требует параметры при запуске.
Возможно не работает с новым форматом конфигов (в которых помимо тестов указаны настройки тестирования).
Также могут возникнуть проблемы при попытке использования больше одной таблицы в СУБД.
Настоятельно рекомендую использовать именно "app_qt", так как это наиболее актуальная и доработанная версия приложения.
'''

import sys
import json
import funcs
import pymysql
import uuid
from serial.tools import list_ports


hlp = '''Если первый параметр - "help", то печатает данный текст.
Если первый параметр - "init", то создаёт файлы с настройками по умолчанию и тестовый конфиг (МОЖЕТ ПЕРЕПИСАТЬ СУЩЕСТВУЮЩИЕ).
Если первый параметр - "coms", то перечисляет активные последовательные порты.
Если первый параметр - "COMn", где n - это номер порта, на который будет отправляться сигнал, а второй параметр - "COMm",
где m - это номер порта, с которого будет читаться сигнал, то запускает тестирование, открывая конфиг с названием по умолчанию.'''


#Список ключей, которые по умолчанию ожидаются в тестах в конфиге
tests_keys_default = {"delay": "delay",
                    "request": "request",
                    "answer": "answer",
                    "comment": "comment"}

#Дефолтные параметры взаимодействия с СУБД
db_credential_default = {"type": "MySQL",
                        "db": "PCB-testing",
                        "login": "root",
                        "pass": "password",
                        "table": "table1",
                        "template": 0,
                        "create": """CREATE TABLE [table] (TestID VARCHAR(36) NOT NULL,
BoardName VARCHAR(255) NOT NULL,
Request VARCHAR(255),
Answer VARCHAR(255),
Time INT NOT NULL,
Comment TEXT,
Date DATETIME NOT NULL) CHARACTER SET=utf8;""",
                        "insert": """INSERT INTO [table] VALUES ('[id]','[name]','[request]','[answer]','[answer_real]',[time],'[comment]','[date]')"""}

#Дефолтный конфиг
config_default = [{"request": "test1&",
                    "answer": "answer1",
                    "delay": 1000},
                    {"request": "test2&",
                    "answer": "answer2",
                    "comment": "второй тест, без задержки чтения, но с запросом комментария от пользователя"}]

#Дефолтные настройки тестирования
no_qt_settings_default = {"skip_comment": 0,
                          "db_or_json": "db",
                          "config": "config.json",
                          "delimiter": '\n',
                          "delay": 3}

#Дефолтная конструкция создания таблицы в СУБД
default_create = f"""CREATE TABLE [table] (
    TestID VARCHAR(36) NOT NULL,
    BoardName VARCHAR(255) NOT NULL,
    Date DATETIME NOT NULL,
    Request VARCHAR(255),
    Answer VARCHAR(255),
    AnswerReal VARCHAR(255),
    Time INT NOT NULL,
    Comment TEXT
) CHARACTER SET=utf8"""

#Дефолтная конструкция внесения данных в СУБД
default_insert = f"""INSERT INTO [table] VALUES (
    '[id]',
    '[name]',
    '[date]',
    '[request]',
    '[answer]',
    '[answer_real]',
    [time],
    '[comment]')"""

args = sys.argv
if len(args) == 1:
    print("Ошибка. Не указаны параметры. Используйте 'help', чтобы узнать перечень возможных параметров.")
else:
    mode = args[1]
    #Заведомо известно, что параметр не может быть короче 4 символов
    #Проверка, чтобы не сломать программу на чтении параметра "COMn"
    if len(mode) < 4:
        print("Ошибка. Введённый параметр неверен. Используйте 'help', чтобы узнать перечень возможных параметров.")
    elif mode == "help":
        print(hlp)
    elif mode == "init":
        #Создание дефолтных файлов
        fl = open("tests_keys.json", "w", encoding='utf-8')
        json.dump(tests_keys_default, fl, ensure_ascii=False)
        fl.close()
        fl = open("config.json", "w", encoding='utf-8')
        json.dump(config_default, fl, ensure_ascii=False)
        fl.close()
        fl = open("db_credential.json", "w", encoding='utf-8')
        json.dump(db_credential_default, fl, ensure_ascii=False)
        fl.close()
        fl = open("no_qt_settings.json", "w", encoding='utf-8')
        json.dump(no_qt_settings_default, fl, ensure_ascii=False)
        fl.close()
    elif mode == "coms":
        #Вывод доступных для подключения портов
        ports = list_ports.comports()
        for i in range(len(ports)):
            print(ports[i].device, ports[i].hwid)
    elif mode[:3] == "COM":
        #Проверка на наличие второго порта в списке параметров
        if len(args) < 3:
            print("Ошибка. Не введён порт для считывания ответного сигнала. Используйте 'help', чтобы узнать перечень возможных параметров.")
            sys.exit()
        #Попытка чтения номеров портов
        try:
            com1 = int(mode[3:])
            com2 = int(args[2][3:])
        except Exception:
            print("Ошибка. Введённый параметр неверен. Используйте 'help', чтобы узнать перечень возможных параметров.")
            sys.exit()

        #Создание словаря с настройками тестирования на основе дефолтных настроек
        no_qt_settings = no_qt_settings_default.copy()
        #Попытка заполнить словарь с настройками тестирования данными из соответсвующего файла (файл должен существовать)
        try:
            fl = open("no_qt_settings.json", "r", encoding='utf-8')
            temp = json.load(fl)
            fl.close()
            for i in temp.keys():
                if i in no_qt_settings.keys():
                    no_qt_settings[i] = temp[i]
            print("Файл с настройками программы найден и прочтён.")
        except Exception:
            print("Ошибка. Файл с настройками программы повреждён или не был найден.")
            print("Программа прервала своё выполнение и создала файл со значениями по умолчанию.")
            fl = open("no_qt_settings.json", "w", encoding='utf-8')
            json.dump(no_qt_settings, fl, ensure_ascii=False)
            fl.close()
            sys.exit()

        #Создание словаря с ключами тестов в конфиге на основе дефолтных ключей
        tests_keys = tests_keys_default.copy()
        #Попытка заполнить словарь с ключами тестов в конфиге данными из соответсвующего файла (файл должен существовать)
        try:
            fl = open("tests_keys.json", "r", encoding='utf-8')
            temp = json.load(fl)
            fl.close()
            for i in temp.keys():
                if i in tests_keys.keys():
                    tests_keys[i] = temp[i]
            print("Файл с ключами конфига найден и прочтён.")
        except Exception:
            print("Ошибка. Файл с ключами конфига повреждён или не был найден.")
            print("Программа прервала своё выполнение и создала файл со значениями по умолчанию.")
            fl = open("tests_keys.json", "w", encoding='utf-8')
            json.dump(tests_keys, fl, ensure_ascii=False)
            fl.close()

        #Создание словаря с тестами через функцию поиска конфига
        config = funcs.stage_config_find(tests_keys, no_qt_settings["config"])
        match config:
            case 1:
                print("Ошибка. Конфиг не получается найти или открыть.")
                sys.exit()
            case 2:
                print("Ошибка. Конфиг содержит неожиданные команды.")
                sys.exit()
            case 3:
                print("Ошибка. Вторым параметром ожидается путь к файлу в формате JSON или DHALL.")
                sys.exit()
            case _:
                print("Конфиг успешно найден и прочтён.")

        #Подключение к первому порту
        ser1 = funcs.stage_connect("COM"+str(com1))
        if not ser1:
            print("Подключение через порт для отправки сигналов не удалось.")
            sys.exit()

        #Подключение ко второму порту (или его копирование, если один порт используется дважды)
        if com1 != com2:
            ser2 = funcs.stage_connect("COM"+str(com2))
            if not ser2:
                print("Подключение через порт для приёма сигналов не удалось.")
                sys.exit()
        else:
            ser2 = ser1

        #Тестирование через функцию итерирования конфига
        print("Тестирование начинается.")
        ans = funcs.stage_config_inter(config[0], ser1, ser2, bool(no_qt_settings["skip_comment"]), tests_keys, float(no_qt_settings["delay"]), no_qt_settings["delimiter"])
        print("Тестирование завершено.")

        #Сохранение результатов тестирования
        if no_qt_settings["db_or_json"] == "json":
            fl = open("results.json", 'w', encoding='utf-8')
            json.dump(ans, fl, ensure_ascii=False)
            fl.close()
            print("Результаты тестирования сохранены в файл 'results.json'.")
        elif no_qt_settings["db_or_json"] == "db":
            #Создание словаря с параметрами подключения к СУБД на основе дефолтных параметров
            db_credential = db_credential_default.copy()
            #Попытка заполнить словарь с параметрами подключения к СУБД данными из соответсвующего файла (файл должен существовать)
            try:
                fl = open("db_credential.json", "r", encoding='utf-8')
                temp = json.load(fl)
                fl.close()
                for i in temp.keys():
                    if i in db_credential.keys():
                        db_credential[i] = temp[i]
                print("Файл с настройками СУБД найден и прочтён.")
            except Exception:
                print("Ошибка. Файл с настройками СУБД был не найден или повреждён.")
                print("Программа прервала своё выполнение и создала файл со значениями по умолчанию.")
                fl = open("db_credential.json", "w", encoding='utf-8')
                json.dump(db_credential, fl, ensure_ascii=False)
                fl.close()
                sys.exit()
            
            if (db_credential["type"] == "MySQL"):
                #MySQL
                #Подключение к СУБД
                try:
                    connection = pymysql.connect(
                        host='localhost',
                        user=db_credential["login"],
                        password=db_credential["pass"],
                        charset='utf8'
                    )
                except Exception as e:
                    #print(e)
                    print("Ошибка. Проверьте логин и пароль.")
                    sys.exit()
                with connection.cursor() as cursor:
                    #Поиск базы данных в СУБД. Если не найдена, то предложение создать. Если найдена, то переключение курсора на неё.
                    cursor.execute("SHOW DATABASES")
                    results = cursor.fetchall()
                    if not ((db_credential["db"].lower(),) in results):
                        dial = ""
                        while not (dial in ["y", "n"]):
                            print("Предупреждение. Указанная база данных не существует. Создать базу данных?")
                            dial = input("Y/N: ")
                            dial = dial.lower()
                        if dial == "y":
                            try:
                                cursor.execute(f"CREATE DATABASE `{db_credential['db']}`;")
                            except Exception as e:
                                print(e)
                                print("Ошибка. Проверьте указанное название базы данных.")
                                connection.close()
                                sys.exit()
                        else:
                            connection.close()
                            sys.exit()
                    cursor.execute(f"USE `{db_credential['db']}`;")
                    #Поиск таблицы в базе данных. Если не найдена, то предложение создать.
                    cursor.execute("SHOW TABLES")
                    results = cursor.fetchall()
                    if not ((db_credential['table'].lower(),) in results):
                        dial = ""
                        while not (dial in ["y", "n"]):
                            print("Предупреждение. Указанная таблица не существует. Создать таблицу?")
                            dial = input("Y/N: ")
                            dial = dial.lower()
                        if dial == "y":
                            try:
                                if bool(db_credential["template"]):
                                    query = db_credential["create"]
                                else:
                                    query = default_create
                                ind = query.find("[table]")
                                if ind != -1:
                                    query = query[:ind] + '`' + db_credential['table'] + '`' + query[ind+7:]
                                cursor.execute(query)
                            except Exception as e:
                                #print(query)
                                print(e)
                                print("Ошибка. Проверьте название или конструктор таблицы.")
                                connection.close()
                                sys.exit()
                        else:
                            connection.close()
                            sys.exit()
                    #Формирование случайного идентификатора
                    test_id = uuid.uuid4()
                    #Попытка чтения конструкций и их использования для занесения данных в СУБД
                    try:
                        #Создание шаблона запроса для занесения данных
                        if bool(db_credential["template"]):
                            query_clean = db_credential["insert"]
                        else:
                            query_clean = default_insert
                        #Перечисление результатов тестов и создание на основе каждого запроса с данными для отправки в СУБД
                        for i in ans:
                            #Создание "чистого" (без данных) запроса на основе шаблона
                            query = query_clean
                            for j in ["table", "id", "name", "date", "request", "answer", "answer_real", "time", "comment"]:
                                ind = query.find("["+j+"]")
                                if ind != -1:
                                    if j == "table":
                                        temp = '`' + db_credential["table"] + '`'
                                    elif j == "id":
                                        temp = test_id
                                    else:
                                        temp = i[j]
                                    query = query[:ind] + str(temp) + query[ind+len(j)+2:]
                            cursor.execute(query)
                        #Применение изменений на стороне СУБД
                        connection.commit()
                    except Exception as e:
                        print(e)
                        print("Ошибка. Проверьте данные или шаблон для добавления данных в таблицу.")
                        connection.close()
                        sys.exit()
                connection.close()
                print("Данные успешно занесены в базу данных.")
                print(f"ID: {test_id}")
    else:
        print("Ошибка. Введённый параметр неверен. Используйте 'help', чтобы узнать перечень возможных параметров.")
