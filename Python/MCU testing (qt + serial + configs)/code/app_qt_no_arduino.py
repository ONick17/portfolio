'''
Программа, запускающая копию приложения с интерфейсом для тестирования.
Вместо настоящей платы использует фальшивку, созданную в классе "ArduinoLike".
Выбор порта подключения в приложении ни на что не влияет, всегда используется "ArduinoLike".
'''

from PyQt5 import QtWidgets
from qt_main import MyMainWindow
import sys

from qt_answers import MyAnswersWindow
import codecs
import time
import types
import json


class ArduinoLike:
    def __init__(self):
        self.mesIn = ""
        self.mesOut = ""
        self.flag = '&'
    
    def main(self):
        mes = self.mesIn
        self.mesIn = ""
        if mes == "":
            self.mesOut = "null\n"
        else:
            self.mesOut = bin(int(mes))[2:] + "\n"

    def write(self, symb: chr):
        symb = symb.decode()
        if symb == self.flag:
            self.main()
        elif (symb.isdigit()):
            self.mesIn += symb

    def readline(self):
        mes = self.mesOut
        self.mesOut = ""
        return mes[:-1].encode()
    
    def read(self):
        mes = ""
        if len(self.mesOut) > 0:
            mes = self.mesOut[0]
            self.mesOut = self.mesOut[1:]
        return mes.encode()



'''
Переписыванние функции тестирования специально под "ArduinoLike".
'''
def stage_start(self):
    #Проверка наличия и корректности необходимых для тестирования данных в редактируемых полях или в файлах с настройками
    #Проверка наличия пути до конфига
    if self.tests == None:
        self.lbl_error.setText("Выберите конфиг")
    else:
        #Проверка наличия делимитра
        if self.settings["delimiter"] == "":
            delimiter = self.lnedit_select_delimiter.text()
        else:
            delimiter = self.settings["delimiter"]
        delimiter = codecs.decode(delimiter, 'unicode_escape')
        #Проверка наличия времени ожидания
        if self.settings["wait"] == "":
            try:
                wait = float(self.lnedit_select_delay.text())
            except Exception:
                self.lbl_error.setText("Недопустимые символы в поле 'Время ожидания'")
                return
        else:
            try:
                wait = float(self.settings["wait"])
            except Exception:
                self.lbl_error.setText("Недопустимые символы в поле 'wait'")
                return
        #Проверка корректности делимитра
        if len(delimiter) > 1:
            self.lbl_error.setText("Делимитер должен быть одним символом")
        elif len(delimiter) == 0:
            self.lbl_error.setText("Делимитер не указан")
        else:
            self.lbl_error.setText("")
            self.anss = []
            cnt = -1
            msgBox = QtWidgets.QMessageBox()
            #Чтение настроек с конфига
            #Если в конфиге не было настройки, то она берётся из интерфейса
            if self.settings["comment_ignore"] == "":
                comment_ignore = self.chkbox_comment_ignore.isChecked()
            else:
                comment_ignore = bool(self.settings["comment_ignore"])
            self.date = time.strftime('%Y-%m-%d %H:%M:%S')
            #Запрос имени устройства
            if self.settings["name"] == "":
                self.name, ok = QtWidgets.QInputDialog.getText(None, 'Введите название', "Введите наименование тестируемого устройства")
                if not ok:
                    self.name = "no_name"
            else:
                self.name = self.settings["name"]

            #Чтение ожидаемых ключей из файла
            #Если с файла считать невозможно, то используются дефолтные ключи
            tests_keys = {"delay": "delay",
                            "request": "request",
                            "answer": "answer",
                            "comment": "comment"}
            try:
                fl_keke = open("tests_keys.json", "r", encoding='utf-8')
                keke = json.load(fl_keke)
                fl_keke.close()
                for i in keke.keys():
                    if i in tests_keys.keys():
                        tests_keys[i] = keke[i]
            except Exception:
                fl_keke = open("tests_keys.json", "w", encoding='utf-8')
                json.dump(tests_keys, fl_keke, ensure_ascii=False)
                fl_keke.close()

            #Проверка наличия настроек чтения конфига в самом конфиге
            if self.settings["delay"] != "":
                tests_keys["delay"] = self.settings["delay"]
            if self.settings["request"] != "":
                tests_keys["request"] = self.settings["request"]
            if self.settings["answer"] != "":
                tests_keys["answer"] = self.settings["answer"]
            if self.settings["comment"] != "":
                tests_keys["comment"] = self.settings["comment"]

            #Создание фальшивой Ардуины
            arduino = ArduinoLike()
            self.time_all = time.time()

            #Итерирование списка тестов
            for i in self.tests:
                #Обнуление флага об отсутствии подключения, увеличение порядкового номера,
                #создание переменной под время теста и словаря с ключами результата
                cnt += 1
                ans = {"count": cnt,
                    "request": "",
                    "time_one": 0,
                    "answer_real": "",
                    "answer": "",
                    "comment": ""}
                err = False
                tm_com = 0

                #Проверка требования ввода комментария, а также вывод предупреждения
                if tests_keys["comment"] in i.keys():
                    comment = True
                    comment_value = i[tests_keys["comment"]]
                    if not comment_ignore:
                        msgBox.setIcon(QtWidgets.QMessageBox.Information)
                        msgBox.setText(
f"""Для следующей команды потребуется ввод комментария.
Пояснение: {comment_value}
Нажмите 'ОК', когда будете готовы, чтобы продолжить.""")
                        msgBox.setWindowTitle("Предупреждение")
                        msgBox.setStandardButtons(QtWidgets.QMessageBox.Ok)
                        #msgBox.show()
                        msgBox.exec_()
                else:
                    comment = False
                
                #Отправка сигнала на устройство, перезаписывание времени начала выполнения команды и проверка подключения
                if tests_keys["request"] in i.keys():
                    ans["request"] = i[tests_keys["request"]]
                    for k in ans["request"]:
                        arduino.write(k.encode())
                        tm_com = time.time()
                
                #Пауза
                if tests_keys["delay"] in i.keys():
                    time.sleep(i[tests_keys["delay"]]/1000)

                #Попытка считывания ответного сигнала, длящаяся пока не придёт делимитер или пока не выйдет время, указанное в delay
                #Отчёт времени до delay обнуляется при поступлении любого сигнала от устройства
                if tests_keys["answer"] in i.keys():
                    ans["answer"] = i[tests_keys["answer"]]
                    if err:
                        ans["answer_real"] = "(lost connection)"
                    else:
                        ans["answer_real"] = ""
                        tm = time.time()
                        while True:
                            data = arduino.read().decode()
                            if len(data) != 0:
                                tm = time.time()
                                if data == delimiter:
                                    break
                                else:
                                    ans["answer_real"] += data
                            elif time.time()-tm > wait:
                                break
                        #Подсчёт времени, затраченного на выполнение команды
                        if tm_com != 0:
                            ans["time"] = int((time.time() - tm_com)*1000)
                #Запрос комментария, если требуется
                if comment:
                    if comment_ignore:
                        ans["comment"] = "ignored"
                    else:
                        text, ok = QtWidgets.QInputDialog.getText(None, 'Комментарий',
    f"""Номер:\t\t\t{ans["count"]}
Команда:\t\t{ans["request"]}
Ожидаемый ответ:\t{ans["answer"]}
Реальный ответ:\t{ans["answer_real"]}
Затраченное время:\t{ans["time"]}
Пояснение:\t\t{comment_value}""")
                        if ok:
                            ans["comment"] = text
                        else:
                            ans["comment"] = "cancel"
                self.anss.append(ans)

            #Подсчёт общего времени выполнения
            self.time_all = time.time() - self.time_all

            #Проверка наличия настроек подключения к СУБД в самом конфиге
            db_settings = {"create": "", "insert_once": "", "insert": "", "type": "", "db": "", "login": "", "pass": "", "table": "", "template": ""}
            db_settings["create"] = self.settings["create"]
            db_settings["insert_once"] = self.settings["insert_once"]
            db_settings["insert"] = self.settings["insert"]
            db_settings["type"] = self.settings["type"]
            db_settings["db"] = self.settings["db"]
            db_settings["login"] = self.settings["login"]
            db_settings["pass"] = self.settings["pass"]
            db_settings["table"] = self.settings["table"]
            db_settings["template"] = self.settings["template"]
            
            #Создание и вывод окна с результатами, в которое передаются результаты, общее время тестирования, дата и наименование устройства
            answers_window = QtWidgets.QDialog()
            answers_window.ui = MyAnswersWindow(self.anss, self.time_all, self.date, self.name, db_settings)
            answers_window.ui.setupUi(answers_window)
            answers_window.exec_()
            #answers_window.show()
            #и exec, и show отображают окно, из-за чего двойное их использование заставляет окно закрываться только со второго раза
            #в чём между ними разница я пока не увидел, так что оставляю только exec
            #upd: видимо show просто показывает контент и сразу же продолжает код, а exec сначала ждёт когда показанный контент закроется


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    main_window = MyMainWindow()
    #Оригинальная функция тестирования заменяется на функцию тестирования, созданную для "ArduinoLike".
    main_window.stage_start = types.MethodType(stage_start, main_window)
    main_window.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
