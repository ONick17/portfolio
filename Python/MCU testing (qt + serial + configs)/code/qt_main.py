'''
Интерфейс главного окна.
Помимо настроек главного окна присутствуют всего его функции.
Функция итерирования конфига и проведения тестирования была вынесена из funcs.py и переписана, так как
требует многократное и плотное взаимодействие с интерфейсом.
'''

from PyQt5 import QtCore, QtGui, QtWidgets
from serial.tools import list_ports
from qt_answers import MyAnswersWindow
from qt_main_db import MySettingsDBWindow
from qt_main_config import MySettingsConfigWindow
import codecs
import funcs
import time
import json

#Вспомогательный класс, помогающий настроить и подключить некоторые функции к окнам выбора порта
class MyComboBox(QtWidgets.QComboBox):
    popupAboutToBeShown = QtCore.pyqtSignal()
    def showPopup(self):
        self.popupAboutToBeShown.emit()
        super(MyComboBox, self).showPopup()


#Класс с интерфейсом главного окна
class MyMainWindow(object):
    def __init__(self):
        #tests - список с тестами
        self.tests = None
        #settings - настройки тестирования
        self.settings = None

    #Создание интерфейса
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(500, 250)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.btn_start = QtWidgets.QPushButton(self.centralwidget)
        self.btn_start.setGeometry(QtCore.QRect(150, 10, 200, 50))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.btn_start.setFont(font)
        self.btn_start.setStyleSheet("")
        self.btn_start.setObjectName("btn_start")
        self.lbl_select_com1 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_select_com1.setGeometry(QtCore.QRect(50, 70, 130, 20))
        self.lbl_select_com1.setObjectName("lbl_select_com1")
        self.lbl_select_config = QtWidgets.QLabel(self.centralwidget)
        self.lbl_select_config.setGeometry(QtCore.QRect(50, 120, 130, 20))
        self.lbl_select_config.setObjectName("lbl_select_config")
        self.lstbox_select_com1 = MyComboBox(self.centralwidget)
        self.lstbox_select_com1.setGeometry(QtCore.QRect(190, 70, 250, 20))
        self.lstbox_select_com1.setObjectName("lstbox_select_com1")
        self.lbl_error = QtWidgets.QLabel(self.centralwidget)
        self.lbl_error.setGeometry(QtCore.QRect(50, 190, 400, 20))
        self.lbl_error.setStyleSheet("color: red;")
        self.lbl_error.setTextFormat(QtCore.Qt.PlainText)
        self.lbl_error.setAlignment(QtCore.Qt.AlignCenter)
        self.lbl_error.setObjectName("lbl_error")
        self.lnedit_select_config = QtWidgets.QLineEdit(self.centralwidget)
        self.lnedit_select_config.setGeometry(QtCore.QRect(190, 120, 180, 20))
        self.lnedit_select_config.setObjectName("lnedit_select_config")
        self.btn_select_config = QtWidgets.QPushButton(self.centralwidget)
        self.btn_select_config.setGeometry(QtCore.QRect(370, 120, 70, 20))
        self.btn_select_config.setObjectName("btn_select_config")
        self.chkbox_comment_ignore = QtWidgets.QCheckBox(self.centralwidget)
        self.chkbox_comment_ignore.setGeometry(QtCore.QRect(150, 170, 200, 20))
        self.chkbox_comment_ignore.setObjectName("chkbox_comment_ignore")
        self.lbl_select_com2 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_select_com2.setGeometry(QtCore.QRect(50, 95, 130, 20))
        self.lbl_select_com2.setObjectName("lbl_select_com2")
        self.lstbox_select_com2 = MyComboBox(self.centralwidget)
        self.lstbox_select_com2.setGeometry(QtCore.QRect(190, 95, 250, 20))
        self.lstbox_select_com2.setObjectName("lstbox_select_com2")
        self.lbl_select_delimiter = QtWidgets.QLabel(self.centralwidget)
        self.lbl_select_delimiter.setGeometry(QtCore.QRect(50, 145, 71, 20))
        self.lbl_select_delimiter.setObjectName("lbl_select_delimiter")
        self.lnedit_select_delimiter = QtWidgets.QLineEdit(self.centralwidget)
        self.lnedit_select_delimiter.setGeometry(QtCore.QRect(120, 145, 71, 20))
        self.lnedit_select_delimiter.setObjectName("lnedit_select_delimiter")
        self.lbl_select_delay = QtWidgets.QLabel(self.centralwidget)
        self.lbl_select_delay.setGeometry(QtCore.QRect(205, 145, 105, 20))
        self.lbl_select_delay.setObjectName("lbl_select_delay")
        self.lnedit_select_delay = QtWidgets.QLineEdit(self.centralwidget)
        self.lnedit_select_delay.setGeometry(QtCore.QRect(310, 145, 130, 20))
        self.lnedit_select_delay.setObjectName("lnedit_select_delay")
        MainWindow.setCentralWidget(self.centralwidget)

        '''
        #Доступ к разным меню настроек
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 500, 26))
        self.menubar.setObjectName("menubar")
        self.menu_settings = QtWidgets.QMenu(self.menubar)
        self.menu_settings.setObjectName("menu_settings")
        MainWindow.setMenuBar(self.menubar)
        self.menu_settings_db = QtWidgets.QAction(MainWindow)
        self.menu_settings_db.setObjectName("menu_settings_db")
        self.menu_settings_config = QtWidgets.QAction(MainWindow)
        self.menu_settings_config.setObjectName("menu_settings_config")
        self.menu_settings.addAction(self.menu_settings_db)
        self.menu_settings.addAction(self.menu_settings_config)
        self.menubar.addAction(self.menu_settings.menuAction())
        '''

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        self.lstbox_select_com1.addItem("(пусто)")
        self.lstbox_select_com1.setCurrentIndex(0)
        self.lstbox_select_com2.addItem("(пусто)")
        self.lstbox_select_com2.setCurrentIndex(0)
        self.add_functions()


    #Подключение функций к элементам интерфейса
    def add_functions(self):
        self.btn_start.clicked.connect(self.stage_start)
        self.lstbox_select_com1.popupAboutToBeShown.connect(self.populateCombo1)
        self.lstbox_select_com2.popupAboutToBeShown.connect(self.populateCombo2)
        self.btn_select_config.clicked.connect(self.browse_files)
        self.lnedit_select_config.textChanged.connect(self.check_file)
        '''
        #Доступ к разным меню настроек
        self.menu_settings_db.triggered.connect(self.settingsDB)
        self.menu_settings_config.triggered.connect(self.settingsConfig)
        '''

    #Создание перечня доступных для подключения портов в выпадающем окне 1
    def populateCombo1(self):
        self.lstbox_select_com1.clear()
        items = ["(пусто)"] + [str(x) for x in list_ports.comports()]
        self.lstbox_select_com1.addItems(items)

    #Создание перечня доступных для подключения портов в выпадающем окне 2
    def populateCombo2(self):
        self.lstbox_select_com2.clear()
        items = ["(пусто)"] + [str(x) for x in list_ports.comports()]
        self.lstbox_select_com2.addItems(items)

    #Создание окна настроек подключения к СУБД
    def settingsDB(self):
        settingsDB_window = QtWidgets.QDialog()
        settingsDB_window.ui = MySettingsDBWindow()
        settingsDB_window.ui.setupUi(settingsDB_window)
        settingsDB_window.exec_()

    #Создание окна настроек чтения конфигов
    def settingsConfig(self):
        settingsConfig_window = QtWidgets.QDialog()
        settingsConfig_window.ui = MySettingsConfigWindow()
        settingsConfig_window.ui.setupUi(settingsConfig_window)
        settingsConfig_window.exec_()

    #Присвоение элементам интерфейса надписей или дефолтных значений
    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Main Window"))
        self.btn_start.setText(_translate("MainWindow", "ЗАПУСК"))
        self.lbl_select_com1.setText(_translate("MainWindow", "Устройство ввода"))
        self.lbl_select_config.setText(_translate("MainWindow", "Конфиг"))
        self.lbl_error.setText(_translate("MainWindow", ""))
        self.btn_select_config.setText(_translate("MainWindow", "Указать"))
        self.chkbox_comment_ignore.setText(_translate("MainWindow", "Игнорировать комментарии"))
        self.lbl_select_com2.setText(_translate("MainWindow", "Устройство вывода"))
        self.lbl_select_delimiter.setText(_translate("MainWindow", "Делимитер"))
        self.lbl_select_delay.setText(_translate("MainWindow", "Время ожидания"))
        '''
        #Доступ к разным меню настроек
        self.menu_settings.setTitle(_translate("MainWindow", "Настройки"))
        self.menu_settings_db.setText(_translate("MainWindow", "СУБД"))
        self.menu_settings_config.setText(_translate("MainWindow", "Конфиг"))
        '''
        self.lnedit_select_delimiter.setText(_translate("MainWindow", r"\n"))
        self.lnedit_select_delay.setText(_translate("MainWindow", "3"))
    
    #Открытие проводника для выбора файла
    def browse_files(self):
        fl = QtWidgets.QFileDialog.getOpenFileName(None, "Choose config", "", "(*.dhall *.json)")
        self.lnedit_select_config.setText(fl[0])

    #Автоматическая проверка указанного файла на корректность
    def check_file(self):
        #Пытается считать путь к файлу
        fl = self.lnedit_select_config.text()
        if (fl == "") or (fl.isspace()):
            self.lbl_error.setText("")
        else:
            #Чтение ожидаемых ключей из файла
            #Если с файла считать невозможно, то используются дефолтные ключи
            tests_keys = {"delay": "delay",
                            "request": "request",
                            "answer": "answer",
                            "comment": "comment"}
            try:
                fl_keke = open("tests_keys.json", "r", encoding='utf-8')
                keke = json.load(fl_keke)
                fl_keke.close()
                for i in tests_keys.keys():
                    if i in keke.keys():
                        tests_keys[i] = keke[i]
            except Exception:
                fl_keke = open("tests_keys.json", "w", encoding='utf-8')
                json.dump(tests_keys, fl_keke, ensure_ascii=False)
                fl_keke.close()

            #Отправляет полученные ключи и файл на проверку
            #В случае успеха разделяет конфиг на тесты (self.tests) и на настройки тестирования (self.settings)
            config = funcs.stage_config_find(tests_keys, fl)
            match config:
                case 1:
                    self.lbl_error.setText("Конфиг не получается найти или открыть")
                case 2:
                    self.lbl_error.setText("Конфиг содержит неожиданные команды")
                case 3:
                    self.lbl_error.setText("Ожидается путь к файлу в формате JSON или DHALL")
                case _:
                    self.lbl_error.setText("")
                    self.tests = config[0]
                    self.settings = config[1]

    #Тестирование
    def stage_start(self):
        #Проверка наличия и корректности необходимых для тестирования данных в редактируемых полях или в файлах с настройками
        #Проверка наличия пути до конфига
        if self.tests == None:
            self.lbl_error.setText("Выберите конфиг")
        else:
            #Проверка наличия делимитра
            if self.settings["delimiter"] == "":
                delimiter = self.lnedit_select_delimiter.text()
            else:
                delimiter = self.settings["delimiter"]
            delimiter = codecs.decode(delimiter, 'unicode_escape')
            #Проверка наличия времени ожидания
            if self.settings["wait"] == "":
                try:
                    wait = float(self.lnedit_select_delay.text())
                except Exception:
                    self.lbl_error.setText("Недопустимые символы в поле 'Время ожидания'")
                    return
            else:
                try:
                    wait = float(self.settings["wait"])
                except Exception:
                    self.lbl_error.setText("Недопустимые символы в поле 'wait'")
                    return
            #Проверка корректности делимитра
            if len(delimiter) > 1:
                self.lbl_error.setText("Делимитер должен быть одним символом")
            elif len(delimiter) == 0:
                self.lbl_error.setText("Делимитер не указан")
            else:
                #Попытка подключения к указанным портам
                port_in = self.lstbox_select_com1.currentText()
                port_out = self.lstbox_select_com2.currentText()
                if (port_in in ["", "(пусто)"]) or (port_out in ["", "(пусто)"]):
                    self.lbl_error.setText("Укажите порт")
                else:
                    self.lbl_error.setText("")
                    port_in = port_in[:port_in.find(' ')]
                    port_out = port_out[:port_out.find(' ')]
                    device_in = funcs.stage_connect(port_in)
                    if port_in == port_out:
                        device_out = device_in
                    else:
                        device_out = funcs.stage_connect(port_out)
                    if not device_in:
                        self.lbl_error.setText("Порт ввода недоступен")
                    elif not device_out:
                        self.lbl_error.setText("Порт вывода недоступен")
                    else:
                        while device_in.in_waiting:  # Or: while ser.inWaiting():
                            #print(device_in.readline())
                            pass
                        #TODO Без time.sleep Serial тупо не успевает подключиться, из-за чего самый первый тест не получает ответа.
                        #TODO Пробовал использовал device_in.isOpen и device_in.open(). Не помогло.
                        #TODO Пока что использую костыль в виде time.sleep(2). За две секунды всё успевает подключиться.
                        time.sleep(2)
                        anss = []
                        cnt = -1
                        msgBox = QtWidgets.QMessageBox()
                        #Чтение настроек с конфига
                        #Если в конфиге не было настройки, то она берётся из интерфейса
                        if self.settings["comment_ignore"] == "":
                            comment_ignore = self.chkbox_comment_ignore.isChecked()
                        else:
                            comment_ignore = bool(self.settings["comment_ignore"])
                        date = time.strftime('%Y-%m-%d %H:%M:%S')

                        #Чтение ожидаемых ключей из файла
                        #Если с файла считать невозможно, то используются дефолтные ключи
                        tests_keys = {"delay": "delay",
                                        "request": "request",
                                        "answer": "answer",
                                        "comment": "comment"}
                        try:
                            fl_keke = open("tests_keys.json", "r", encoding='utf-8')
                            keke = json.load(fl_keke)
                            fl_keke.close()
                            for i in keke.keys():
                                if i in tests_keys.keys():
                                    tests_keys[i] = keke[i]
                        except Exception:
                            fl_keke = open("tests_keys.json", "w", encoding='utf-8')
                            json.dump(tests_keys, fl_keke, ensure_ascii=False)
                            fl_keke.close()

                        #Проверка наличия настроек чтения конфига в самом конфиге
                        if self.settings["delay"] != "":
                            tests_keys["delay"] = self.settings["delay"]
                        if self.settings["request"] != "":
                            tests_keys["request"] = self.settings["request"]
                        if self.settings["answer"] != "":
                            tests_keys["answer"] = self.settings["answer"]
                        if self.settings["comment"] != "":
                            tests_keys["comment"] = self.settings["comment"]
                        
                        #вставить сюда окно статуса с кнопкой отмены, секундомером и номером/названием выполняющейся команды
                        time_all = time.time()

                        #Запрос имени устройства
                        if self.settings["name"] == "":
                            name, ok = QtWidgets.QInputDialog.getText(None, 'Введите название', "Введите наименование тестируемого устройства")
                            if not ok:
                                name = "no_name"
                        else:
                            name = self.settings["name"]

                        #Итерирование списка тестов
                        for i in self.tests:
                            #Обнуление флага об отсутствии подключения, увеличение порядкового номера,
                            #создание переменной под время теста и словаря с ключами результата
                            cnt += 1
                            ans = {"count": cnt,
                                    "request": "",
                                    "time_one": 0,
                                    "answer_real": "",
                                    "answer": "",
                                    "comment": ""}
                            err = False
                            tm_com = 0

                            #Проверка требования ввода комментария, а также вывод предупреждения
                            if tests_keys["comment"] in i.keys():
                                comment = True
                                comment_value = i[tests_keys["comment"]]
                                if not comment_ignore:
                                    msgBox.setIcon(QtWidgets.QMessageBox.Information)
                                    msgBox.setText(
f"""Для следующей команды потребуется ввод комментария.
Пояснение: {comment_value}
Нажмите 'ОК', когда будете готовы, чтобы продолжить.""")
                                    msgBox.setWindowTitle("Предупреждение")
                                    msgBox.setStandardButtons(QtWidgets.QMessageBox.Ok)
                                    #msgBox.show()
                                    msgBox.exec_()
                            else:
                                comment = False

                            #Отправка сигнала на устройство, перезаписывание времени начала выполнения команды и проверка подключения
                            if tests_keys["request"] in i.keys():
                                ans["request"] = i[tests_keys["request"]]
                                try:
                                    tm_com = time.time()
                                    device_in.write(ans["request"].encode())
                                except Exception:
                                    err = True

                            #Пауза
                            if tests_keys["delay"] in i.keys():
                                time.sleep(i[tests_keys["delay"]]/1000)
                            
                            #Попытка считывания ответного сигнала, длящаяся пока не придёт делимитер или пока не выйдет время, указанное в delay
                            #Отчёт времени до delay обнуляется при поступлении любого сигнала от устройства
                            if tests_keys["answer"] in i.keys():
                                ans["answer"] = i[tests_keys["answer"]]
                                if err:
                                    ans["answer_real"] = "(lost connection)"
                                else:
                                    try:
                                        ans["answer_real"] = ""
                                        tm_brk = time.time()
                                        while True:
                                            if device_out.in_waiting == 0:
                                                if time.time()-tm_brk > wait:
                                                    break
                                            else:
                                                data = device_out.read().decode()
                                                tm_brk = time.time()
                                                if data == delimiter:
                                                    break
                                                else:
                                                    ans["answer_real"] += data
                                    except Exception:
                                        ans["answer_real"] = "(lost connection)"
                                    #Подсчёт времени, затраченного на выполнение команды
                                    if tm_com != 0:
                                        ans["time"] = int((time.time() - tm_com)*1000)
                            #Запрос комментария, если требуется
                            if comment:
                                if comment_ignore:
                                    ans["comment"] = "ignored"
                                else:
                                    text, ok = QtWidgets.QInputDialog.getText(None, 'Комментарий',
f"""Номер:\t\t\t{ans["count"]}
Команда:\t\t{ans["request"]}
Ожидаемый ответ:\t{ans["answer"]}
Реальный ответ:\t{ans["answer_real"]}
Затраченное время:\t{ans["time"]}
Пояснение:\t\t{comment_value}""")
                                    if ok:
                                        ans["comment"] = text
                                    else:
                                        ans["comment"] = "cancel"
                            anss.append(ans)

                        #Подсчёт общего времени выполнения
                        time_all = time.time() - time_all

                        device_in.close()
                        if device_out != device_in:
                            device_out.close()

                        #Проверка наличия настроек подключения к СУБД в самом конфиге
                        db_settings = {"create": "", "insert_once": "", "insert": "", "type": "", "db": "", "login": "", "pass": "", "table": "", "template": ""}
                        db_settings["create"] = self.settings["create"]
                        db_settings["insert_once"] = self.settings["insert_once"]
                        db_settings["insert"] = self.settings["insert"]
                        db_settings["type"] = self.settings["type"]
                        db_settings["db"] = self.settings["db"]
                        db_settings["login"] = self.settings["login"]
                        db_settings["pass"] = self.settings["pass"]
                        db_settings["table"] = self.settings["table"]
                        db_settings["template"] = self.settings["template"]

                        #Создание и вывод окна с результатами, в которое передаются результаты, общее время тестирования, дата и наименование устройства
                        answers_window = QtWidgets.QDialog()
                        answers_window.ui = MyAnswersWindow(anss, time_all, date, name, db_settings)
                        answers_window.ui.setupUi(answers_window)
                        answers_window.exec_()
                        #answers_window.show()
                        #и exec, и show отображают окно, из-за чего двойное их использование заставляет окно закрываться только со второго раза
                        #в чём между ними разница я пока не увидел, так что оставляю только exec
                        #upd: видимо show просто показывает контент и сразу же продолжает код, а exec сначала ждёт когда показанный контент закроется
