'''
Базовые функции для работы приложения.
Поиск, проверка и чтение конфига.
Поиск, проверка и создание подключения к плате.
Итерирование конфига и проведение тестиования (используется только в app_no_qt)
'''

from serial.tools import list_ports
import serial as srl
import json as js
import dhall as dh
import time
JSON = "config.json"
DHALL = "config.dhall"
DEVICE = None
CONFIG = None
COMMENT = True


#Проверка переданного конфига (списка из словарей) на правильность написания (просматривает ключи, не трогает значения)
#Доступные ключи хранятся в переданном словаре tests_keys
#Если находит несоответствие, возвращает False
#Если все ключи верны, возвращает список, который состоит из tests и settings
#В tests входят тесты, которые надо провести
#В settings входят настройки тестирования, которые были найдены в конфиге
def config_check(config, tests_keys):
    settings = {"name": "", "delimiter": "", "wait": "", "comment_ignore": "", 
                "delay": "", "request": "", "answer": "", "comment": "", 
                "create": "", "insert_once": "", "insert": "", "type": "", "db": "", "login": "", "pass": "", "table": "", "template": ""}
    #Если переданный объект является словарём, то программа ищет в нём ключ "tests" и забирает его значение в tests
    #Если ключ "tests" не удаётся найти, то возвращается ошибка
    #Также программа ищет ключ "settings". Если находит, то считывает значения (ожидается словарь) и сохраняет в settings
    if type(config) == dict:
        if "tests" in config.keys():
            tests = config["tests"]
            if "settings" in config.keys():
                if type(config["settings"]) == dict:
                    for i in settings.keys():
                        if i in config["settings"].keys():
                            settings[i] = config["settings"][i]
        else:
            return False
    #Если переданный объект не является словарём, то ожидается, что это список из тестов, который программа сохраняет в tests
    else:
        tests = config
    #Проверка списка tests из тестов (каждый тест - это словарь) на правильные ключи
    for i in tests:
        for j in i.keys():
            if (j not in tests_keys.values()):
                return False
    return [tests, settings]


#Поиск конфига по переданному пути или в папке с исполняемым файлом
#Если находит, считывает данные из файла и передаёт их в функцию config_check, где проверяется соответствие ключей тестов с tests_keys,
#а также собираются настройки тестирования, если таковые присутствовали в конфиге
#Если проверка проходит удачно, то возвращает config - список, в котором содержатся список с тестами и словарь с настойками тестирования
#Если не нашёл файл или проверка ключей не увенчалась успехом, возвращает код ошибки
#1 - не удалось найти или прочитать файл
#2 - в файле содержится неожиданная команда
#3 - ошибка в написании пути к файлу
def stage_config_find(tests_keys, path_to_config = ".\\config.dhall"):
    #print(tests_keys)
    #print(path_to_config)
    #Название файла не может быть короче 6 символов
    #Проверка, дабы программа не сломалась от дальнейших сабстрингов
    if len(path_to_config) < 6:
        #print("Error: bad input, use dhall or json file.")
        return 3
    #Проверка расширения файлов
    if path_to_config[-6:].find(".json") != -1:
        type = "json"
    elif path_to_config[-6:].find(".dhall") != -1:
        type = "dhall"
    else:
        #print("Error: bad input, use dhall or json file.")
        return 3
    if type == "json":
        try:
            config = js.load(open(path_to_config, encoding='utf-8'))
        except Exception:
            #print("Error: can't open or find file.")
            return 1
    elif type == "dhall":
        try:
            config = dh.load(open(path_to_config, encoding='utf-8'))
        except Exception:
            #print("Error: can't open or find file.")
            return 1
    config = config_check(config, tests_keys)
    if not config:
        #print("Error: unexpected commands in config.")
        return 2
    else:
        return config


#Перечисление доступных для подключения портов и просьба о выборе
#Если всё успешно, возвращает port - наименование выбранного порта
#Иначе, письмо об ошибке и возврат False
#Данная функция перестала где-либо использоваться
def stage_find_port():
    ports = list_ports.comports()
    print("Found ports:")
    for i in range(len(ports)):
        print(ports[i].device, ports[i].hwid)
        ports[i] = ports[i].device
    port = input("Choose port: ").upper()
    if port in ports:
        return(port)
    else:
        #print("Error: chosen port is not available.")
        return(False)

#Подключение к устройству через переданный порт
#Если всё успешно, возвращает device - объект Serial
#Иначе, письмо об ошибке и возврат False
def stage_connect(port):
    try:
        device = srl.Serial(port)
        return(device)
    except Exception:
        #print("Error: device is not available.")
        return(False)


#Проверка наличия соединения между компом и устройством
#True, если подключение есть
#False, если подключения нет
def check_connect(device):
    for i in list_ports.comports():
        if (i.device == device.port):
            return True
    return False


#Итерирование конфига
#config - список с тестами
#device_in и device_out - объекты Serial, по которым отправляются и читаются данные соответственно
#comment_ignore - флаг игнорирования комментариев
#tests_keys - ключи, которые возможны в тестах
#delay - врема ожидания ответа от устройства
#delimiter - символ, разделяющий входящие данные
#Подключение с устройством проверяется перед каждой отправкой или чтением данных
#Если в момент итерации теста подключения нет, то тест пропускается, а в результат записывается предупреждение о разрыве связи
#Возвращает результаты тестирования в виде списка (anss) словарей (ans)
def stage_config_inter(config, device_in, device_out, comment_ignore, tests_keys, delay=3, delimiter='\n'):\
    #TODO Без time.sleep Serial тупо не успевает подключиться, из-за чего самый первый тест не получает ответа.
    #TODO Пробовал использовал device_in.isOpen и device_in.open(). Не помогло.
    #TODO Пока что использую костыль в виде time.sleep(2). За две секунды всё успевает подключиться.
    time.sleep(2)
    anss = []
    cnt = -1
    date = time.strftime('%Y-%m-%d %H:%M:%S')
    #Итерирование списка тестов
    for i in config:
        #Обнуление флага об отсутствии подключения и увеличение порядкового номера
        err = False
        cnt += 1
        #Ключи, из которых состоит словарь с результатом итерации
        ans = {"count": cnt,
                "date": date,
                "request": "",
                "time": 0,
                "answer_real": "",
                "answer": "",
                "comment": "",
                "name": device_in.name}
        #Время начала выполнения команды и общее время выполнения теста (на случай, если в тесте нет ключей "request" или "answer")
        tm_com = time.time()
        ans["time"] = 0
        #Проверка требования ввода комментария, а также вывод предупреждения
        if tests_keys["comment"] in i.keys():
            comment = True
            if not comment_ignore:
                print("Следующий тест требует ввод комментария.")
                print("Пояснение к комментарию:", i[tests_keys["comment"]])
                input("Нажмите 'ввод', чтобы продолжить.")
        else:
            comment = False
        #Отправка сигнала на устройство, перезаписывание времени начала выполнения команды и проверка подключения
        if tests_keys["request"] in i.keys():
            ans["request"] = i[tests_keys["request"]]
            if check_connect(device_in):
                tm_com = time.time()
                device_in.write(ans["request"].encode())
            else:
                err = True
        #Пауза
        if tests_keys["delay"] in i.keys():
            time.sleep(i[tests_keys["delay"]]/1000)
        #Попытка считывания ответного сигнала, длящаяся пока не придёт делимитер или пока не выйдет время, указанное в delay
        #Отчёт времени до delay обнуляется при поступлении любого сигнала от устройства
        if tests_keys["answer"] in i.keys():
            ans["answer"] = i[tests_keys["answer"]]
            if err:
                ans["answer_real"] = "обрыв связи при вводе"
            else:
                if check_connect(device_out):
                    ans["answer_real"] = ""
                    tm_brk = time.time()
                    while True:
                        if device_out.in_waiting == 0:
                            if time.time()-tm_brk > delay:
                                break
                        else:
                            data = device_out.read().decode()
                            tm_brk = time.time()
                            if data == delimiter:
                                break
                            else:
                                ans["answer_real"] += data
                else:
                    ans["answer_real"] = "обрыв связи при выводе"
            #Подсчёт времени, затраченного на выполнение команды
            ans["time"] = int((time.time() - tm_com)*1000)
        #Запрос комментария, если требуется
        if comment:
            if comment_ignore:
                ans["comment"] = "ignored"
            else:
                print(f'Команда:\t\t{ans["request"]}')
                print(f'Ожидаемый ответ:\t{ans["answer"]}')
                print(f'Реальный ответ:\t{ans["answer_real"]}')
                ans["comment"] = input("Комментарий: ")
        anss.append(ans)
    device_in.close()
    device_out.close()
    return anss
