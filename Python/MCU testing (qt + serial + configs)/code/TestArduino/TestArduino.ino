String mes = "";

void setup() {
  Serial.begin(9600);
}

String ten_to_two(String mes) {
  String ans = "";
  int c, k;
  int n = mes.toInt();
  for(c = 7; c >= 0; c--){
    k = n >> c;
    if (k == 0) {
      continue;
    } else if (k & 1){
        ans += "1";
    } else {
        ans += "0";
    }
  }
  return ans;
}

void loop() {
  if (Serial.available() > 0) {
    char inp = Serial.read();
    if (inp == '&') {
      if (mes == "") {
        Serial.print("null\n");
      } else {
        mes = ten_to_two(mes);
        Serial.print(mes + "\n");
      }
      mes = "";
    } else {
      if (isDigit(inp)){
        mes = mes + inp;
      }
    }
  }
}
