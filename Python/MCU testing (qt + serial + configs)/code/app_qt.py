'''
Программа, запускающая приложение с интерфейсом.
'''

from PyQt5 import QtWidgets
from qt_main import MyMainWindow
import sys


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    main_window = MyMainWindow()
    main_window.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
