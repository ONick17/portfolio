from tkinter import ttk
from tkinter import messagebox as ms
from tkinter import *
from tkinter import scrolledtext
from tkinter import Menu
from tkinter.filedialog import askopenfilename
from tkinter.filedialog import asksaveasfilename
from idlelib.tooltip import Hovertip
import tkinter.simpledialog
import tkinter as tk
import docx


#Функция выхода из программы при закрытии окна
def close():
    window.quit()
    window.destroy()
    raise SystemExit



def file():
    global lbl_slct_fl1
    global doc_name
    doc_name = askopenfilename(filetypes=(("Word-files", "*.docx"), ("all files", "*.*")))
    # Если пользователь не выбрал файл, название будет измененно на "none"
    if doc_name == "":
        doc_name = "none"
    # Отображение пути к документу в окне программы
    elif ((doc_name[-5:-1] + doc_name[-1]) != ".docx"):
        ms.showerror("Ошибка", "Выберите документ в формате .docx!")
    if (len(doc_name)>25):
        short_dir = "..." + (doc_name[-22:])
        lbl_slct_fl1.configure(text=short_dir, width=25)
    else:
        lbl_slct_fl1.configure(text=doc_name)
    Hovertip(lbl_slct_fl1, doc_name)
    return (doc_name)



def shablon():
    global lbl_slct_fl2
    global shablon_name
    # Tk.withdraw()
    shablon_name = askopenfilename(filetypes = (("Word-files","*.docx"),("all files","*.*")))
    # Если пользователь не выбрал файл, название будет измененно на "none"
    if shablon_name == "":
        shablon_name = "none"
    elif ((shablon_name[-5:-1] + shablon_name[-1]) != ".docx"):
        ms.showerror("Ошибка", "Выберите шаблон в формате .docx!")
    # Отображение пути к шаблону в окне программы
    if (len(shablon_name)>25):
        short_dir = "..." + (shablon_name[-22:])
        lbl_slct_fl2.configure(text=short_dir, width=25)
    else:
        lbl_slct_fl2.configure(text=shablon_name)
    Hovertip(lbl_slct_fl2, shablon_name)
    return (shablon_name)



#Функция обработки события при нажатии на кнопку "Выполнить"
def btn_do_clck():
    global shablon_name
    global doc_name
    #Проверка документа и вывод комментария на экран
    if (doc_name == "none"):
        ms.showerror("Ошибка", "Выберите документ.")
    #Проверка шаблона и вывод комментария на экран
    elif (shablon_name == "none"):
        ms.showerror("Ошибка", "Выберите шаблон.")
    #Копирование текста из документа в шаблон, вывод текста на экран и в новый файл
    else:
        new_doc_name = asksaveasfilename(defaultextension=".docx")
        if (new_doc_name != ""):
            main_algorithm(doc_name, shablon_name, new_doc_name)



def about():
    newWindow = tk.Toplevel(window)
    newWindow.geometry('400x400')
    text = "Приложение, разработанное для изменения РПД. "
    info = tk.Text(newWindow, height=400, width = 400)
    info.pack()
    info.insert(tk.END,text)
    info.configure(state='disabled')
    newWindow.title("О программе")



def helpl():
    newWindow = tk.Toplevel(window)
    newWindow.geometry('400x400')
    text = "1. Выберите файл со старой РПД" '\n' \
           "2. Выберите Ваш новый шаблон" '\n' \
           "3. Вберите место сохранение нового файла и ввидите имя"
    info = tk.Text(newWindow, height=400, width = 400)
    info.pack()
    info.insert(tk.END,text)
    info.configure(state='disabled')
    newWindow.title("Инструкция")



def text_check(doc, shablon):
    i = -1
    for prgr in shablon.paragraphs:
        i += 1
        rns = prgr.runs
        hd_text = ""
        hd2_text = ""
        new_text = ""
        mode = 1
        if (prgr.text != ""): 
            for rn in prgr.runs:
                if (rn.font.highlight_color == None) and (mode == 1):
                    hd_text += rn.text
                elif (rn.font.highlight_color != None) and ((mode == 1) or (mode == 2)):
                    mode = 2
                elif (rn.font.highlight_color == None) and ((mode == 2) or (mode == 3)):
                    if (rn.text != "."):
                        mode = 3
                    hd2_text += rn.text
                elif (rn.font.highlight_color != None) and (mode == 3):
                    mode = 2
                    new_text = find_rn(doc, i, hd_text, hd2_text)
                    prgr.text = prgr.text[:prgr.text.find(hd_text)+len(hd_text)] + new_text + prgr.text[prgr.text.find(hd2_text):]
                    hd_text = hd2_text
                    hd2_text = ""
            if (mode == 1):
                prgr.text = hd_text
            elif (mode == 2):
                new_text = find_rn(doc, i, hd_text, hd2_text)
                prgr.text = prgr.text[:prgr.text.find(hd_text)+len(hd_text)] + new_text + hd2_text
            elif (mode == 3):
                new_text = find_rn(doc, i, hd_text, hd2_text)
                prgr.text = prgr.text[:prgr.text.find(hd_text)+len(hd_text)] + new_text + prgr.text[prgr.text.find(hd2_text):]
    return(shablon)



def find_rn(doc, i, hd_text, hd2_text):
    if (i < len(doc.paragraphs)):
        if (doc.paragraphs[i].text.find(hd_text) != -1):
            if (doc.paragraphs[i].text.find(hd2_text) != -1) and (hd2_text != "."):
                new_text = doc.paragraphs[i].text[doc.paragraphs[i].text.find(hd_text)+len(hd_text):doc.paragraphs[i].text.find(hd2_text)]
            elif (doc.paragraphs[i].text.find(hd2_text) != -1):
                new_text = doc.paragraphs[i].text[doc.paragraphs[i].text.find(hd_text)+len(hd_text):-1]
            else:
                new_text = tkinter.simpledialog.askstring("ТРЕБУЕТСЯ ВВОД", "Предшествующий текст:" + '\n' +hd_text + '\n' + ' '*100)
                if new_text == None:
                    new_text = ""
        else:
            new_text = find_rn2(doc, hd_text, hd2_text)
    else:
        new_text = find_rn2(doc, hd_text, hd2_text)
    return(new_text)



def find_rn2(doc, hd_text, hd2_text):
    for prgr in doc.paragraphs:
        if (prgr.text.find(hd_text) != -1):
            if (prgr.text.find(hd2_text) != -1):
                new_text = prgr.text[prgr.text.find(hd_text)+len(hd_text):prgr.text.find(hd2_text)]
                return(new_text)
            else:
                new_text = tkinter.simpledialog.askstring("ТРЕБУЕТСЯ ВВОД", "Предшествующий текст:" + '\n' +hd_text + '\n' + ' '*100)
                if new_text == None:
                    new_text = ""
                return(new_text)
    new_text = tkinter.simpledialog.askstring("ТРЕБУЕТСЯ ВВОД", "Предшествующий текст:" + '\n' +hd_text + '\n' + ' '*100)
    if new_text == None:
        new_text = ""
    return(new_text)



def ask_window2(num, tbl, i, j):
    new_text = tkinter.simpledialog.askstring("ТРЕБУЕТСЯ ВВОД",
                                              "Номер таблицы: " + str(num+1) + '\n' +
                                              "Текст строки: " + tbl.rows[i].cells[0].text + '\n' +
                                              "Текст столбца: " + tbl.rows[0].cells[j].text + '\n' +
                                              ' '*150)
    if new_text == None:
        new_text = ""
    return(new_text)



def ask_window3(num, tbl, i, j):
    new_text = tkinter.simpledialog.askstring("ТРЕБУЕТСЯ ВВОД",
                                              "Номер таблицы: " + str(num+1) + '\n' +
                                              "Номер строки: " + str(i+1) + '\n' +
                                              "Текст столбца: " + tbl.rows[0].cells[j].text + '\n' +
                                              ' '*150)
    if new_text == None:
        new_text = ""
    return(new_text)



def ask_window4(num, tbl, i, j):
    new_text = tkinter.simpledialog.askstring("ТРЕБУЕТСЯ ВВОД",
                                              "Номер таблицы: " + str(num+1) + '\n' +
                                              "Номер строки: " + "1" + '\n' +
                                              "Номер столбца: " + str(j+1) + '\n' +
                                              ' '*150)
    if new_text == None:
        new_text = ""
    return(new_text)



def tables_check(doc, shablon):
    num_table = -1
    for tbl in shablon.tables:
        num_table += 1
        hd_texts = []
        for rw in tbl.rows:
            for cl in rw.cells:
                font_chk = False
                for prgr in cl.paragraphs:
                    for rn in prgr.runs:
                        if (rn.font.highlight_color == None):
                            font_chk = True
                if font_chk:
                    hd_texts.append(cl.text)
        cnt_chk = 0
        num_doc_table = -1
        for doc_tbl in doc.tables:
            num_doc_table += 1
            cnt = 0
            for doc_cl in doc_tbl.rows[0].cells:
                for hd_text in hd_texts:
                    if (hd_text == doc_cl.text):
                        cnt += 1
                        break
            for doc_rw in doc_tbl.rows:
                for hd_text in hd_texts:
                    if (hd_text == doc_rw.cells[0].text):
                        cnt += 1
                        break
            cnt = cnt * 100 / len(hd_texts)
            if (cnt >= 70) and (cnt > cnt_chk):
                cnt_chk = cnt
                num_doc_table_chk = num_doc_table
        if (cnt_chk == 0):
            ms.showinfo("Новая таблица!", "Требуется полное заполнение!")
            for i in range(len(tbl.rows)):
                for j in range(len(tbl.rows[i].cells)):
                    font_chk = False
                    for prgr in tbl.rows[i].cells[j].paragraphs:
                        for rn in prgr.runs:
                            if (rn.font.highlight_color != None):
                                font_chk = True
                    if (font_chk):
                        if (i > 0) and (j > 0):
                            tbl.rows[i].cells[j].text = ask_window2(num_doc_table-1, tbl, i, j)
                        elif (i > 0) and (j == 0):
                            tbl.rows[i].cells[j].text = ask_window3(num_doc_table-1, tbl, i, j)
                        else:
                            tbl.rows[i].cells[j].text = ask_window4(num_doc_table-1, tbl, i, j)
        else:
            tbl_hd_chk = False
            for rw in tbl.rows[1:]:
                for prgr in rw.cells[0].paragraphs:
                    for rn in prgr.runs:
                        if (rn.font.highlight_color == None):
                            tbl_hd_chk = True
            if tbl_hd_chk:
                shablon.tables[num_table] = fill_cls2HD(doc.tables[num_doc_table_chk], tbl, num_table)
            else:
                shablon.tables[num_table] = fill_cls1HD(doc.tables[num_doc_table_chk], tbl, num_table)
    return(shablon)



def fill_cls1HD(doc_tbl, tbl, num):
    if (len(doc_tbl.rows) == len(tbl.rows)):
        for i in range(len(doc_tbl.rows)):
            tbl.rows[i].cells[0].text = doc_tbl.rows[i].cells[0].text
        for i in range(1, len(doc_tbl.rows)):
            for j in range(1, len(doc_tbl.rows[i].cells)):
                tbl.rows[i].cells[j].text = find_cl(doc_tbl, tbl, i, j)
                if (tbl.rows[i].cells[j].text == "NT_FND"):
                    tbl.rows[i].cells[j].text = ask_window2(num, tbl, i, j)
        return(tbl)
    else:
        for i in range(1, len(doc_tbl.rows)):
            tbl.rows[i].cells[0].text = ask_window3(num, tbl, i, j)
            for i in range(1, len(doc_tbl.rows)):
                for j in range(1, len(doc_tbl.rows[i].cells)):
                    tbl.rows[i].cells[j].text = find_cl(doc_tbl, tbl, i, j)
                    if (tbl.rows[i].cells[j].text == "NT_FND"):
                        tbl.rows[i].cells[j].text = ask_window2(num, tbl, i, j)
            return(tbl)



def fill_cls2HD(doc_tbl, tbl, num):
    for i in range(len(tbl.rows)):
        for j in range(len(tbl.rows[i].cells)):
            font_chk = False
            for prgr in tbl.rows[i].cells[j].paragraphs:
                for rn in prgr.runs:
                    if (rn.font.highlight_color != None):
                        font_chk = True
            if (font_chk):
                if (i > 0):
                    if (j > 0):
                        tbl.rows[i].cells[j].text = find_cl(doc_tbl, tbl, i, j)
                        if (tbl.rows[i].cells[j].text == "NT_FND"):
                            tbl.rows[i].cells[j].text = ask_window2(num, tbl, i, j)
                    else:
                        tbl.rows[i].cells[j].text = ask_window3(num, tbl, i, j)
                else:
                    tbl.rows[i].cells[j].text = ask_window4(num, tbl, i, j)
    return(tbl)



def find_cl(doc_table, tbl, i, j):
    clj = -1
    for cl in doc_table.rows[0].cells:
        clj += 1
        if (cl.text == tbl.rows[0].cells[j].text):
            cli = 0
            for doc_rw in doc_table.rows[1:]:
                cli += 1
                if (doc_rw.cells[0].text == tbl.rows[i].cells[0].text):
                    return(doc_table.rows[cli].cells[clj].text)
    return("NT_FND")



def main_algorithm(doc_name, shablon_name, new_doc_name):
    shablon = text_check(docx.Document(doc_name), docx.Document(shablon_name))
    shablon = tables_check(docx.Document(doc_name), shablon)
    shablon.save(new_doc_name)



#Создание окна
window = Tk()
window.title("Temple")
window.geometry("")
#Элементы меню
menu = Menu(window)
new_item = Menu(menu,tearoff=0)
file_item=Menu(menu,tearoff=0)
new_item.add_command(label='О программе',command=about)
new_item.add_command(label='Инструкция',command=helpl)
#####
file_item.add_command(label='Окрыть документ',command=file)
file_item.add_command(label='Окрыть шаблон',command=shablon)
file_item.add_separator()
file_item.add_command(label='Выход', command=window.destroy)
#####
menu.add_cascade(label='Файл', menu=file_item)
menu.add_cascade(label='Справка', menu=new_item)
window.config(menu=menu)
#Создание фрэйма для документа
frm_slct1 = LabelFrame(window, text="Документ")
#Заполнение фрэйма для выбора документа
doc_name = "none"
btn_slct_fl1 = Button(frm_slct1, text="Выбрать файл", font=("Times", 20 ), justify=CENTER, background="#140f0b", foreground="#ffffff",width=12, height=2, command=file)
lbl_slct_fl1 = Label(frm_slct1, text=doc_name)
lbl_slct_fl1.grid(ipady=10)
btn_slct_fl1.grid(pady=10)
frm_slct1.grid(column=1,row=1)
#Фрэйм для шаблона
shablon_name = "none"
frm_slct2 = LabelFrame(window, text="Шаблон")
#Заполнение фрэйма для шаблона
btn_slct_fl2 = Button(frm_slct2, text="Выбрать файл", font=("Times", 20 ), justify=CENTER, background="#140f0b", foreground="#ffffff",width=12, height=2, command=shablon)
lbl_slct_fl2 = Label(frm_slct2, text=shablon_name)
lbl_slct_fl2.grid(ipady=10)
btn_slct_fl2.grid(pady=10)
frm_slct2.grid(column=2, row=1)
#Кнопка выполнить
frm_do = Frame(window)
btn_do = Button(frm_do, text="Выполнить", font=("Times", 20 ), background="#140f0b", foreground="#ffffff", width=12, height=2, command=btn_do_clck)
btn_do.grid()
frm_do.grid(column=1,row=2, columnspan=2, ipadx=20, pady=15)
#Завершение программы при закрытии окна
window.protocol("WM_DELETE_WINDOW", close)
#Функция для работы окна
window.mainloop()
