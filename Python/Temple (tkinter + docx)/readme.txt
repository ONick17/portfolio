Программа по редактированию документа под новый шаблон.

БЕТА-ВЕРСИЯ

Сама программа находится в файле "Temple.py".

Все остальные файлы являются либо старыми версиями алгоритма (*.py), либо
файлами для проверки работы программы (*.docx).

Программа до сих пор не дописана, а конкретно:
- форматирования отдельных участков не сохраняются, а меняются на общий стиль абзаца;
- пропускаются запросы на ввод, не содержащие текст до и после них;
- пропускаются запросы на ввод, не содержащие текст после них (баг);
- пропадают разрывы страниц;
- после создания нового документа не обновляется оглавление;
- плохо считывает таблицы с объединёнными ячейками;
- пропускает запрос на ввод в списках.

На данный момент программа может:
- копировать неизменяемые картинки, параграфы, списки и таблицы из шаблона
в новый документ;
- находить, если это является возможным, в старом документе информацию, которую
нужно подставить в новый документ, и подставлять её;
- запрашивать у пользователя ввод данных для их подстановки, если нужная
информация не была найдена в оригинальном документе;
- передавать в новый документ форматирование параграфов;
- находить в старом документе таблицы, наиболее подходящие под описание таблиц
из шаблона;
- создавать в новом документе новые таблицы (если они есть в шаблоне, но нет в
оригинальном документе) и заполнять их с помощью ввода пользователя.

Список библиотек:
- Tkinter (интерфейс);
- python-docx (работа с файлами *.docx);
- tkinter.easydialog (метод для tkinter, позволяющий легко создавать запросы).

Причина создания программы:
Летняя практика в вузе ИГУ на первом курсе в 2021 году.