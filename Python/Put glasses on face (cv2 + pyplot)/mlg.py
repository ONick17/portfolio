import cv2
import numpy as np
import matplotlib.pyplot as plt


cv2.namedWindow("MLG DETECTOR", cv2.WINDOW_AUTOSIZE)
cam = cv2.VideoCapture(0)
cam.set(cv2.CAP_PROP_EXPOSURE, -4)
width = int(cam.get(cv2.CAP_PROP_FRAME_WIDTH))
height = int(cam.get(cv2.CAP_PROP_FRAME_HEIGHT))
mlg = cv2.imread("mlg.png", cv2.IMREAD_UNCHANGED)

def detector(img, classifier, scale=None, min_nbs=None):
    result = img.copy()
    rects = classifier.detectMultiScale(result, scaleFactor=scale,
                                        minNeighbors=min_nbs)
    rects2 = []
    for i in range(0, len(rects), 2):
        if i != len(rects)-1:
            if rects[i][1] < rects[i+1][1]:
                y1 = rects[i][1]
                y2 = rects[i+1][1] + rects[i+1][3]
            else:
                y1 = rects[i+1][1]
                y2 = rects[i][1] + rects[i][3]
            if rects[i][0] < rects[i+1][0]:
                x1 = rects[i][0]
                x2 = rects[i+1][0] + rects[i+1][2]
            else:
                x1 = rects[i+1][0]
                x2 = rects[i][0] + rects[i][2]
            if (x1-x2 != 0) and (y1-y2 != 0):
                rects2.append([x1, x2, y1, y2])
    for (x1, x2, y1, y2) in rects2:
        if x1>10:
            x1 -= 10
        if x2<width-10:
            x2 += 10
        if y1>10:
            y1 -= 10
        if y2<height-10:
            y2 += 10
        #print(x1, x2, y1, y2)
        mlg2 = cv2.resize(mlg, (abs(x2-x1), abs(y2-y1)), interpolation = cv2.INTER_AREA)
        mlg_mask = np.where(mlg2[:, :, -1])
        mlg2 = mlg2[:, :, :-1]
        
        result[y1:y2, x1:x2][mlg_mask] = mlg2[mlg_mask]
    return result


eye = cv2.CascadeClassifier("haarcascade_eye.xml")
glasses = cv2.CascadeClassifier("haarcascade_eye_tree_eyeglasses.xml")

ret, frame_old = cam.read()
while cam.isOpened():
    ret, frame = cam.read()
    frame = detector(frame, eye, 1.2, 5)
    cv2.imshow("MLG DETECTOR", frame)
    key = cv2.waitKey(10)
    if key > 0:
        if chr(key) == 'q':
            break
cv2.destroyAllWindows()
