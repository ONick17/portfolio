extends Control


@onready var HP_sprite1 = $marginHP/HP1
@onready var HP_sprite2 = $marginHP/HP2
@onready var HP_sprite3 = $marginHP/HP3
@onready var message_label = $margin_message/message


const hp0 = preload("res://assets/UI/hp/h_hp0.png")
const hp1 = preload("res://assets/UI/hp/h_hp1.png")
const hp2 = preload("res://assets/UI/hp/h_hp2.png")


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


var message_timer = 0.0
var message_time = 0.0
var message_timer_check = true
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if message_label.text:
		if message_timer_check:
			message_timer += delta
			if message_timer > message_time:
				message_timer = 0.0
				message_label.text = ""


func set_hp(hp):
	HP_sprite1.texture = hp0
	HP_sprite2.texture = hp0
	HP_sprite3.texture = hp0
	if hp > 1:
		HP_sprite1.texture = hp2
		if hp > 3:
			HP_sprite2.texture = hp2
			if hp > 5:
				HP_sprite3.texture = hp2
			elif hp == 5:
				HP_sprite3.texture = hp1
		elif hp == 3:
			HP_sprite2.texture = hp1
	elif hp == 1:
		HP_sprite1.texture = hp1


func set_hp_blink(hp):
	set_hp(hp)
	HP_sprite1.modulate = Color.RED
	HP_sprite2.modulate = Color.RED
	HP_sprite3.modulate = Color.RED
	await get_tree().create_timer(0.1).timeout
	HP_sprite1.modulate = Color.WHITE
	HP_sprite2.modulate = Color.WHITE
	HP_sprite3.modulate = Color.WHITE
	await get_tree().create_timer(0.1).timeout
	HP_sprite1.modulate = Color.RED
	HP_sprite2.modulate = Color.RED
	HP_sprite3.modulate = Color.RED
	await get_tree().create_timer(0.1).timeout
	HP_sprite1.modulate = Color.WHITE
	HP_sprite2.modulate = Color.WHITE
	HP_sprite3.modulate = Color.WHITE
	await get_tree().create_timer(0.1).timeout
	HP_sprite1.modulate = Color.RED
	HP_sprite2.modulate = Color.RED
	HP_sprite3.modulate = Color.RED
	await get_tree().create_timer(0.1).timeout
	HP_sprite1.modulate = Color.WHITE
	HP_sprite2.modulate = Color.WHITE
	HP_sprite3.modulate = Color.WHITE


func show_message(message, time, color=Color.WHITE):
	if time == 117:
		message_timer_check = false
	else:
		message_timer_check = true
		message_time = time
	message_label.text = message
	message_label.modulate = color
