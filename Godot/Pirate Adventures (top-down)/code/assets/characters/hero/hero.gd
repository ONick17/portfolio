extends CharacterBody2D

#@export var bullet = preload("res://assets/objects/bullet.tscn")
const BULLET = preload("res://assets/objects/bullet.tscn")
const BOMB = preload("res://assets/objects/bomb.tscn")

@onready var player_animation = $animation
@onready var detection_right = $right
@onready var detection_left = $left
@onready var detection_up = $up
@onready var detection_down = $down

const speed = 125
const move_window_time = 0.1
const attack_window_time = 0.5
const bomb_window_time = 2.0
const damage = 1
const max_hp = 6
const message_dead = "You are Dead\nPress Space to exit"



var hud
func _ready():
	hud = get_tree().get_root().get_node("level/camera/ui/hud")
	hud.set_hp(max_hp)



func _process(delta):
	pass



var changeScreen = false
var changeLevel = false
var attack_window_timer = attack_window_time
var bomb_window_timer = bomb_window_time
var move_window_timer = 0.0
var cur_state = "idle"
var cur_direction = "down"
var hp = max_hp
var interactions = []
var damage_allowed = true
var destination = Vector2(0, 0)
var move_len = 32
func _physics_process(delta):
	if Engine.time_scale > 0:
		if (cur_state != "dead"):
			attack_window_timer += delta
			bomb_window_timer += delta
			if cur_state != "moving":
				#movement input + animation
				if Input.is_action_pressed("player_down"):
					move_window_timer += delta
					if (cur_direction == "down") and (not detection_down.is_colliding()) and (move_window_timer > move_window_time):
						destination = self.global_position
						destination.y += 32
						cur_state = "moving"
						player_animation.play()
					else:
						cur_direction = "down"
						player_animation.flip_h = false
						player_animation.play("down")
						player_animation.stop()
				
				elif Input.is_action_pressed("player_up"):
					move_window_timer += delta
					if (cur_direction == "up") and (not detection_up.is_colliding()) and (move_window_timer > move_window_time):
						destination = self.global_position
						destination.y -= 32
						cur_state = "moving"
						player_animation.play()
					else:
						cur_direction = "up"
						player_animation.flip_h = false
						player_animation.play("up")
						player_animation.stop()
				
				elif Input.is_action_pressed("player_left"):
					move_window_timer += delta
					if (cur_direction == "left") and (not detection_left.is_colliding()) and (move_window_timer > move_window_time):
						destination = self.global_position
						destination.x -= 32
						cur_state = "moving"
						player_animation.play()
					else:
						cur_direction = "left"
						player_animation.flip_h = false
						player_animation.play("left")
						player_animation.stop()
				
				elif Input.is_action_pressed("player_right"):
					move_window_timer += delta
					if (cur_direction == "right") and (not detection_right.is_colliding()) and (move_window_timer > move_window_time):
						destination = self.global_position
						destination.x += 32
						cur_state = "moving"
						player_animation.play()
					else:
						cur_direction = "right"
						player_animation.flip_h = true
						player_animation.play("left")
						player_animation.stop()
				
				#idle
				else:
					move_window_timer = 0.0
					if (cur_direction != "right"):
						player_animation.play(cur_direction)
						player_animation.stop()
					else:
						player_animation.play("left")
						player_animation.stop()
				
				#attack
				if Input.is_action_pressed("attack"):
					shoot_bullet()
					
				#bomb
				if Input.is_action_pressed("alt_attack"):
					drop_bomb()

				#interaction
				if Input.is_action_just_released("interact"):
					interact()

			#moving
			else:
				player_move(delta)
				

		#return to menu after death
		elif Input.is_action_just_released("attack"):
			get_tree().change_scene_to_file("res://levels/level_menu.tscn")



func player_move(delta):
	if cur_direction == "up":
		move_len -= snapped(speed*delta, 1)
		if move_len >= 0:
			self.global_position.y -= snapped(speed*delta, 1)
		else:
			self.global_position.y -= snapped(speed*delta, 1) - move_len
	elif cur_direction == "down":
		move_len -= snapped(speed*delta, 1)
		if move_len >= 0:
			self.global_position.y += snapped(speed*delta, 1)
		else:
			self.global_position.y += snapped(speed*delta, 1) - move_len
	elif cur_direction == "left":
		move_len -= snapped(speed*delta, 1)
		if move_len >= 0:
			self.global_position.x -= snapped(speed*delta, 1)
		else:
			self.global_position.x -= snapped(speed*delta, 1) - move_len
	elif cur_direction == "right":
		move_len -= snapped(speed*delta, 1)
		if move_len >= 0:
			self.global_position.x += snapped(speed*delta, 1)
		else:
			self.global_position.x += snapped(speed*delta, 1) - move_len
	
	if move_len <= 0:
		cur_state = "idle"
		self.global_position = destination
		move_len = 32
		if changeScreen:
			#get_tree().get_root().get_node("level").changeScreen(changeScreen)
			changeScreen = false
			get_tree().current_scene.changeScreen()
		elif changeLevel:
			changeLevel = false
			get_tree().current_scene.changeScene()



var bullet
func shoot_bullet():
	if BULLET and attack_window_timer > attack_window_time:
		attack_window_timer = 0.0
		bullet = BULLET.instantiate()
		bullet.creator = self
		get_tree().current_scene.add_child(bullet)
		if cur_direction == "up":
			bullet.global_position = detection_up.global_position
			bullet.rotation = -90
			bullet.direction = Vector2.UP
		elif cur_direction == "right":
			bullet.global_position = detection_right.global_position
		elif cur_direction == "down":
			bullet.global_position = detection_down.global_position
			bullet.rotation = 90
			bullet.direction = Vector2.DOWN
		else:
			bullet.global_position = detection_left.global_position
			bullet.rotation = 180
			bullet.direction = Vector2.LEFT



var bomb
func drop_bomb():
	if BOMB and bomb_window_timer > bomb_window_time:
		bomb_window_timer = 0.0
		bomb = BOMB.instantiate()
		bomb.global_position = self.global_position
		get_tree().current_scene.add_child(bomb)
	
func take_damage(dmg):
	if (cur_state != "dead") and (damage_allowed):
		hp -= dmg
		if hp < 1:
			get_killed()
			return true
		else:
			got_damage(hp)
			return false



func get_killed():
	hud.set_hp(0)
	hud.show_message(message_dead, 117, Color.RED)
	velocity.x = 0
	cur_state = "dead"
	player_animation.modulate = Color.BLACK
	player_animation.stop()
	self.set_collision_layer_value(32, true)
	self.set_collision_layer_value(2, false)
	self.set_collision_mask_value(3, false)
	self.set_collision_mask_value(4, false)
	self.set_collision_mask_value(5, false)



func got_damage(hp):
	damage_allowed = false
	hud.set_hp_blink(hp)
	player_animation.modulate = Color.RED
	await get_tree().create_timer(0.1).timeout
	player_animation.modulate = Color.WHITE
	await get_tree().create_timer(0.1).timeout
	player_animation.modulate = Color.RED
	await get_tree().create_timer(0.1).timeout
	player_animation.modulate = Color.WHITE
	await get_tree().create_timer(0.1).timeout
	player_animation.modulate = Color.RED
	await get_tree().create_timer(0.1).timeout
	player_animation.modulate = Color.WHITE
	player_animation.modulate = Color.RED
	await get_tree().create_timer(0.1).timeout
	player_animation.modulate = Color.WHITE
	await get_tree().create_timer(0.1).timeout
	player_animation.modulate = Color.RED
	await get_tree().create_timer(0.1).timeout
	player_animation.modulate = Color.WHITE
	await get_tree().create_timer(0.1).timeout
	player_animation.modulate = Color.RED
	await get_tree().create_timer(0.1).timeout
	player_animation.modulate = Color.WHITE
	player_animation.modulate = Color.RED
	await get_tree().create_timer(0.1).timeout
	player_animation.modulate = Color.WHITE
	await get_tree().create_timer(0.1).timeout
	player_animation.modulate = Color.RED
	await get_tree().create_timer(0.1).timeout
	player_animation.modulate = Color.WHITE
	await get_tree().create_timer(0.1).timeout
	player_animation.modulate = Color.RED
	await get_tree().create_timer(0.1).timeout
	player_animation.modulate = Color.WHITE
	damage_allowed = true



func _on_player_area_entered(area):
	if area.is_in_group("interaction"):
		interactions.insert(0, area)
		#interaction_label.text = interactions[0].interact_label



func _on_player_area_exited(area):
	if area.is_in_group("interaction"):
		interactions.erase(area)
		if interactions:
			pass
			#interaction_label.text = interactions[0].interact_label
		else:
			pass
			#interaction_label.text = ""



func interact():
	if interactions:
		interactions[0].interact(self)
		interactions.remove_at(0)
		if interactions:
			pass
			#interaction_label.text = interactions[0].interact_label
		else:
			pass
			#interaction_label.text = ""
