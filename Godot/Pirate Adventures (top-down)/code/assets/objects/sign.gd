extends Area2D

@export var direction : String
@export var ghosts_spawn : Array[Vector2]

# Called when the node enters the scene tree for the first time.
func _ready():
	body_entered.connect(_sendSignal)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _sendSignal(body: Node2D):
	if body.name == "hero":
		get_tree().current_scene.direction = direction
		get_tree().current_scene.ghosts_spawn = ghosts_spawn
		body.changeScreen = true
