extends Area2D

const speed = 500
const damage = 1


# Called when the node enters the scene tree for the first time.
func _ready():
	pass



var direction = Vector2.RIGHT
func _physics_process(delta):
	self.global_position += speed * direction * delta



var creator
func _on_body_entered(body):
	if body != creator:
		if body.is_in_group("can_be_damaged"):
			body.take_damage(damage)
		queue_free()



func _screen_exited():
	queue_free()
