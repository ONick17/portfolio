extends Area2D

@export var new_scene : PackedScene
@export var ghosts_spawn : Array[Vector2]
@export var hero_spawn : Vector2

# Called when the node enters the scene tree for the first time.
func _ready():
	body_entered.connect(_sendSignal)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _sendSignal(body: Node2D):
	if body.name == "hero":
		get_tree().current_scene.scene = new_scene
		get_tree().current_scene.hero_spawn = hero_spawn
		get_tree().current_scene.ghosts_spawn = ghosts_spawn
		body.changeLevel = true
