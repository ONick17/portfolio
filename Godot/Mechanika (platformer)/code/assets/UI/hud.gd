extends Control

@onready var HP_sprite = $marginHP/HP
@onready var message_label = $margin_message/message

const hp3 = preload("res://assets/UI/hp/3hp.png")
const hp2 = preload("res://assets/UI/hp/2hp.png")
const hp1 = preload("res://assets/UI/hp/1hp.png")
const hp0 = preload("res://assets/UI/hp/0hp.png")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

var message_timer = 0.0
var message_time = 0.0
var message_timer_check = true
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if message_label.text:
		if message_timer_check:
			message_timer += delta
			if message_timer > message_time:
				message_timer = 0.0
				message_label.text = ""

func set_hp(hp):
	if hp == 3:
		HP_sprite.texture = hp3
	elif hp == 2:
		HP_sprite.texture = hp2
	elif hp == 1:
		HP_sprite.texture = hp1
	elif hp < 1:
		HP_sprite.texture = hp0
		
func set_hp_blink(hp):
	if hp == 3:
		HP_sprite.texture = hp3
	elif hp == 2:
		HP_sprite.texture = hp2
	elif hp == 1:
		HP_sprite.texture = hp1
	elif hp < 1:
		HP_sprite.texture = hp0
	HP_sprite.modulate = Color.RED
	await get_tree().create_timer(0.1).timeout
	HP_sprite.modulate = Color.WHITE
	await get_tree().create_timer(0.1).timeout
	HP_sprite.modulate = Color.RED
	await get_tree().create_timer(0.1).timeout
	HP_sprite.modulate = Color.WHITE
	await get_tree().create_timer(0.1).timeout
	HP_sprite.modulate = Color.RED
	await get_tree().create_timer(0.1).timeout
	HP_sprite.modulate = Color.WHITE
	
func show_message(message, time, color=Color.WHITE):
	if time == 117:
		message_timer_check = false
	else:
		message_timer_check = true
		message_time = time
	message_label.text = message
	message_label.modulate = color
