extends CharacterBody2D

@onready var my_animation = $tube_animation
# Called when the node enters the scene tree for the first time.

var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")
var alive_check = true

func _ready():
	my_animation.frame = 0

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if not alive_check:
		if not is_on_floor():
			velocity.y += gravity * delta
		move_and_slide()

func take_damage(dmg):
	if alive_check:
		if dmg > 1:
			alive_check = false
			my_animation.play("die")
			self.set_collision_layer_value(32, true)
			self.set_collision_layer_value(4, false)
			self.set_collision_mask_value(2, false)
			self.set_collision_mask_value(3, false)
