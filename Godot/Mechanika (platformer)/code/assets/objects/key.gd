extends Area2D

@onready var my_animation = $key_animation

const interact_label = ""
const ready_to_interact = true

# Called when the node enters the scene tree for the first time.
func _ready():
	my_animation.play("idle")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func interact(body):
	body.has_key = true
	self.queue_free()
