extends CharacterBody2D

@onready var player_animation = $player_animation
@onready var wall_detection_right = $wall_detection_right
@onready var wall_detection_left = $wall_detection_left
@onready var stair_detection_right = $stair_detection_right
@onready var stair_detection_left = $stair_detection_left
@onready var fall_detection_right = $fall_detection_right
@onready var fall_detection_left = $fall_detection_left
@onready var hit_collision_right = $hit_area/hit_collision_right
@onready var hit_collision_left = $hit_area/hit_collision_left
@onready var interaction_label = $player_area/interaction_label
#@onready var hud = get_tree().get_root().get_node("$camera/ui/hud")


# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")

const speed = 1000
const fall_speed = 1400
const accel = 2000  * (speed/300.)
const friction = 1500  * (speed/300.)
var jump_force = gravity/-4.5
var jump_charged_force = gravity/-3.5
const jump_boost = 1.05
const jump_boost_time = 1.0
const jump_move_friction = 0.5
const attack_boost_time = 0.5
const attack_window_time = 0.05
const damage_strong = 5
const damage_weak = 1
const max_hp = 3
const message_die = "You are Dead\nPress Space to exit"
const message_key = "Try finding the key"

var spawn_point
var hud
func _ready():
	hud = get_node("/root/level1/player_character/camera/ui/hud")
	hud.set_hp(max_hp)
	spawn_point = self.global_position

func _process(delta):
	pass

var cur_velocity
var jump_pressed_timer = 0.0
var attack_pressed_timer = 0.0
var attack_window_timer = 0.0
var cur_state = "idle"
var attack = 0
var look_right = true
var hp = max_hp
var damage = 1
var jump_charged = false
var has_key = false
var interactions = []
func _physics_process(delta):
	if Engine.time_scale > 0:
		#jump+gravity
		if not is_on_floor():
			#movement on stairs
			if (fall_detection_right.is_colliding() or fall_detection_left.is_colliding()) and not ["jump", "fall"].has(cur_state):
				var right_dis = fall_detection_right.global_transform.origin.distance_to(fall_detection_right.get_collision_point())
				var left_dis = fall_detection_left.global_transform.origin.distance_to(fall_detection_left.get_collision_point())
				if right_dis < left_dis:
					self.global_position.y = fall_detection_right.get_collision_point().y
	#				velocity.y = fall_detection_right.get_collision_point().y
				else:
					self.global_position.y = fall_detection_left.get_collision_point().y
	#				velocity.y = fall_detection_left.get_collision_point().y
			else:
				# Add the gravity.
				velocity.y += gravity * delta
				#limit by max fall speed
				velocity.y = velocity.limit_length(fall_speed).y
				if cur_state != "die":
					#fall/jump animation
					if velocity.y > 0:
						cur_state = "fall"
						player_animation.play("fall")
					else:
						player_animation.play("jump")
					#jump-boost (higher jump when up pressed)
					if jump_pressed_timer < jump_boost_time:
						if Input.is_action_pressed("ui_up"):
							velocity.y *= jump_boost
							jump_pressed_timer += delta
						else:
							jump_pressed_timer = jump_boost_time
		else:
			#jump
			if (cur_state != "attack") and (cur_state != "die"):
				if Input.is_action_pressed("ui_up"):
					cur_state = "jump"
					if jump_charged:
						velocity.y = jump_charged_force
						jump_charged = false
						player_animation.modulate = Color(1, 1, 1)
					else:
						velocity.y = jump_force
					jump_pressed_timer = 0.0
				#movement on stairs
				if not ["jump", "fall"].has(cur_state):
					if stair_detection_right.is_colliding():
						if not wall_detection_right.is_colliding():
							self.global_position.y = stair_detection_right.get_collision_point().y - 1
							self.global_position.x += 2
					if stair_detection_left.is_colliding():
						if not wall_detection_left.is_colliding():
							self.global_position.y = stair_detection_left.get_collision_point().y - 1
							self.global_position.x -= 2
		if (cur_state != "die"):
			if cur_state != "attack":
				#movement
				if Input.is_action_pressed("ui_left") or Input.is_action_pressed("ui_right"):
					#run animation
					if is_on_floor():
						if cur_state != "jump":
							cur_state = "run"
						player_animation.play("run")
					#calculate move
					cur_velocity = accel*delta
					#for less character control in air
					if not is_on_floor():
						cur_velocity *= jump_move_friction
					#apply move + change sprite direction
					if Input.is_action_pressed("ui_right"):
						velocity.x += cur_velocity
						player_animation.flip_h = false
						look_right = true
					else:
						velocity.x -= cur_velocity
						player_animation.flip_h = true
						look_right = false
					#limit by max speed
					velocity.x = velocity.limit_length(speed).x
				else:
					#friction
					if abs(velocity.x) > (friction * delta):
						cur_velocity = friction * delta
						if not is_on_floor():
							cur_velocity *= jump_move_friction
						velocity.x -= velocity.x/abs(velocity.x) * cur_velocity
					else:
						velocity.x = 0
					if is_on_floor():
						#stay-still animation
						if cur_state != "jump":
							cur_state = "idle"
						player_animation.play("idle")

				#attack
				if Input.is_action_pressed("attack"):
					attack_pressed_timer += delta
					#waiting for strong attack
					if attack_pressed_timer > attack_boost_time:
						if is_on_floor():
							velocity.x = 0
							cur_state = "attack"
							player_animation.play("attack_strong")
							attack = 2
							damage = damage_strong
						attack_pressed_timer = 0.0
				#if weak attack
				elif Input.is_action_just_released("attack"):
					if is_on_floor():
						velocity.x = 0
						cur_state = "attack"
						player_animation.play("attack")
						attack = 1
						damage = damage_weak
						#weak_attack
					attack_pressed_timer = 0.0
			else:
				if (player_animation.frame > 2) and (attack > 0):
					if attack != 4:
						if look_right:
							hit_collision_right.disabled = false
						else:
							hit_collision_left.disabled = false
						attack = 4
					attack_window_timer += delta
					if attack_window_timer > attack_window_time:
						if look_right:
							hit_collision_right.disabled = true
						else:
							hit_collision_left.disabled = true
						attack_window_timer = 0.0
						attack = 0
		elif Input.is_action_just_released("attack"):
			#self.global_position = spawn_point
			#velocity.x = 0
			#player_animation.modulate = Color.WHITE
			#hp = 3
			#cur_state = "idle"
			#self.set_collision_layer_value(2, true)
			#self.set_collision_layer_value(32, false)
			#self.set_collision_mask_value(3, true)
			#self.set_collision_mask_value(4, true)
			#self.set_collision_mask_value(5, true)
			get_tree().change_scene_to_file("res://level_menu.tscn")
		
		if Input.is_action_just_released("interact"):
			interact()
		
		move_and_slide()


#hit_area
#нанесение урона
func _on_hit_body_entered(body):
	if body.is_in_group("can_be_hitted"):
		body.take_damage(damage)


#проверка окончания анимации удара
func _on_animation_finished():
	if cur_state == "attack":
		cur_state = "idle"


func take_damage(dmg, vector:Vector2 = Vector2(0, 1)):
	#vector = (vector-Vector2(0, 1))/2+Vector2(0, 1)
	if cur_state != "die":
		#velocity = vector*dmg*gravity/-7
		hp -= dmg
		if hp < 1:
			get_killed()
			return true
		else:
			got_damage(hp)
			return false

func get_killed():
	hud.set_hp(0)
	hud.show_message(message_die, 117, Color.RED)
	velocity.x = 0
	cur_state = "die"
	player_animation.modulate = Color.BLACK
	player_animation.play("idle")
	player_animation.stop()
	self.set_collision_layer_value(32, true)
	self.set_collision_layer_value(2, false)
	self.set_collision_mask_value(3, false)
	self.set_collision_mask_value(4, false)
	self.set_collision_mask_value(5, false)

func got_damage(hp):
	hud.set_hp_blink(hp)
	player_animation.modulate = Color.RED
	await get_tree().create_timer(0.1).timeout
	player_animation.modulate = Color.WHITE
	await get_tree().create_timer(0.1).timeout
	player_animation.modulate = Color.RED
	await get_tree().create_timer(0.1).timeout
	player_animation.modulate = Color.WHITE
	await get_tree().create_timer(0.1).timeout
	player_animation.modulate = Color.RED
	await get_tree().create_timer(0.1).timeout
	player_animation.modulate = Color.WHITE


func charge_jump():
	jump_charged = true
	
	
func _on_player_area_entered(area):
	if (area.is_in_group("booster_jump") and not jump_charged) or (area.name == "door"):
		if area.ready_to_interact:
			interactions.insert(0, area)
			interaction_label.text = interactions[0].interact_label
	elif area.name == "key":
		area.interact(self)


func _on_player_area_exited(area):
	if (area.is_in_group("booster_jump") and not jump_charged) or (area.name == "door"):
		if area.ready_to_interact:
			interactions.erase(area)
			if interactions:
				interaction_label.text = interactions[0].interact_label
			else:
				interaction_label.text = ""


func interact():
	if interactions:
		interactions[0].interact(self)
		interactions.remove_at(0)
		if interactions:
			interaction_label.text = interactions[0].interact_label
		else:
			interaction_label.text = ""

func show_key_message():
	hud.show_message(message_key, 3)
